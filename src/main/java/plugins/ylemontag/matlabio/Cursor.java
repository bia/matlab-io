package plugins.ylemontag.matlabio;

import icy.sequence.Sequence;

import java.io.IOException;

import plugins.ylemontag.matlabio.DimensionMapping;
import plugins.ylemontag.matlabio.IcyDimension;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Object used to browse a sequence, so that the pixels are visited in the
 * order corresponding their logical arrangement in the Matlab array
 */
public abstract class Cursor
{
	/**
	 * Parameter defining which channels should be filled in the targeted sequence 
	 */
	public enum ModeC
	{
		EVEN_C,
		ODD_C ,
		ALL_C ;
	}
	
	private int _sizeX;
	private int _sizeY;
	private int _sizeZ;
	private int _sizeT;
	private int _sizeC;
	private DimensionManager[] _mapping;
	private boolean _isValidPos;
	protected int _posX;
	protected int _posY;
	protected int _posZ;
	protected int _posT;
	protected int _posC;
	protected int _posXY;
	protected Sequence _seq;
	
	@Override
	public String toString()
	{
		return "(x,y,z,t,c)=(" + getX() + "," + getY() + "," + getZ() + "," + getT() + "," + getC() + ")";
	}
	
	/**
	 * Constructor
	 */
	protected Cursor(Sequence seq, DimensionMapping mapping, ModeC modeC)
		throws DimensionMapping.BadMapping
	{
		_sizeX = seq.getSizeX();
		_sizeY = seq.getSizeY();
		_sizeZ = seq.getSizeZ();
		_sizeT = seq.getSizeT();
		_sizeC = seq.getSizeC();
		IcyDimension[] inverseMapping = mapping.getInverseMapping();
		_mapping = new DimensionManager[5];
		for(int k=0; k<inverseMapping.length; ++k) {
			switch(inverseMapping[k]) {
				case X: _mapping[k] = new DimensionManagerX(k); break;
				case Y: _mapping[k] = new DimensionManagerY(k); break;
				case Z: _mapping[k] = new DimensionManagerZ(k); break;
				case T: _mapping[k] = new DimensionManagerT(k); break;
				case C: _mapping[k] = new DimensionManagerC(k, modeC==ModeC.ALL_C ? 1 : 2); break;
			}
		}
		_posX = 0;
		_posY = 0;
		_posZ = 0;
		_posT = 0;
		_posC = modeC==ModeC.ODD_C ? 1 : 0;
		_posXY = 0;
		_seq = seq;
		_isValidPos =
			(_posX<_sizeX) &&
			(_posY<_sizeY) &&
			(_posZ<_sizeZ) &&
			(_posT<_sizeT) &&
			(_posC<_sizeC);
	}
	
	/**
	 * Go to the next pixel
	 */
	public void next()
	{
		_mapping[0].goNext();
	}
	
	/**
	 * Execute the copy action at the current position
	 */
	public abstract void consume() throws IOException;
	
	/**
	 * Return true if the cursor points to a pixel in the sequence
	 */
	public boolean isValidPosition()
	{
		return _isValidPos;
	}
	
	/**
	 * Size of the sequence in the X dimension
	 */
	public int getSizeX()
	{
		return _sizeX;
	}
	
	/**
	 * Size of the sequence in the Y dimension
	 */
	public int getSizeY()
	{
		return _sizeY;
	}
	
	/**
	 * Size of the sequence in the Z dimension
	 */
	public int getSizeZ()
	{
		return _sizeZ;
	}
	
	/**
	 * Size of the sequence in the T dimension
	 */
	public int getSizeT()
	{
		return _sizeT;
	}
	
	/**
	 * Size of the sequence in the C dimension
	 */
	public int getSizeC()
	{
		return _sizeC;
	}
	
	/**
	 * Position X of the cursor
	 */
	public int getX()
	{
		return _posX;
	}
	
	/**
	 * Position Y of the cursor
	 */
	public int getY()
	{
		return _posY;
	}
	
	/**
	 * Position Z of the cursor
	 */
	public int getZ()
	{
		return _posZ;
	}
	
	/**
	 * Position T of the cursor
	 */
	public int getT()
	{
		return _posT;
	}
	
	/**
	 * Position C of the cursor
	 */
	public int getC()
	{
		return _posC;
	}
	
	/**
	 * Refresh the pointers after a change of the Z coordinate
	 */
	protected abstract void notifyPosZChanged();
	
	/**
	 * Refresh the pointers after a change of the T coordinate
	 */
	protected abstract void notifyPosTChanged();
	
	/**
	 * Refresh the pointers after a change of the C coordinate
	 */
	protected abstract void notifyPosCChanged();
	
	/**
	 * Functor for increasing dimensions
	 */
	private abstract class DimensionManager
	{
		private int _nextManager;
		private int _size;
		
		/**
		 * Constructor
		 */
		protected DimensionManager(int order, int size)
		{
			_nextManager = order+1;
			_size = size;
		}
		
		/**
		 * Go to the next pixel
		 */
		public void goNext()
		{
			int newValue = getNextValue();
			if(newValue>=_size) {
				resetValue();
				if(_nextManager>=5) {
					_isValidPos = false;
					return;
				}
				else {
					_mapping[_nextManager].goNext();
				}
			}
			else {
				increaseValue();
			}
		}
		
		/**
		 * Increase the value of the managed coordinate
		 */
		protected abstract void increaseValue();
		
		/**
		 * Set the value of the managed coordinate to 0
		 */
		protected abstract void resetValue();
		
		/**
		 * Return the current value of the managed coordinate
		 */
		protected abstract int getNextValue();
	}
	
	/**
	 * Manager for the X dimension
	 */
	private class DimensionManagerX extends DimensionManager
	{
		public DimensionManagerX(int order)
		{
			super(order, _sizeX);
		}
		
		@Override
		protected void increaseValue()
		{
			_posXY++;
			_posX++;
		}
		
		@Override
		protected void resetValue()
		{
			_posXY -= _posX;
			_posX = 0;
		}

		@Override
		protected int getNextValue()
		{
			return _posX+1;
		}
	}
	
	/**
	 * Manager for the Y dimension
	 */
	private class DimensionManagerY extends DimensionManager
	{
		public DimensionManagerY(int order)
		{
			super(order, _sizeY);
		}
		
		@Override
		protected void increaseValue()
		{
			_posXY += _sizeX;
			_posY++;
		}
		
		@Override
		protected void resetValue()
		{
			_posXY = _posX;
			_posY = 0;
		}

		@Override
		protected int getNextValue()
		{
			return _posY+1;
		}
	}
	
	/**
	 * Manager for the Z dimension
	 */
	private class DimensionManagerZ extends DimensionManager
	{
		public DimensionManagerZ(int order)
		{
			super(order, _sizeZ);
		}
		
		@Override
		protected void increaseValue()
		{
			_posZ++;
			notifyPosZChanged();
		}
		
		@Override
		protected void resetValue()
		{
			_posZ = 0;
			notifyPosZChanged();
		}

		@Override
		protected int getNextValue()
		{
			return _posZ+1;
		}
	}
	
	/**
	 * Manager for the T dimension
	 */
	private class DimensionManagerT extends DimensionManager
	{
		public DimensionManagerT(int order)
		{
			super(order, _sizeT);
		}
		
		@Override
		protected void increaseValue()
		{
			_posT++;
			notifyPosTChanged();
		}
		
		@Override
		protected void resetValue()
		{
			_posT = 0;
			notifyPosTChanged();
		}

		@Override
		protected int getNextValue()
		{
			return _posT+1;
		}
	}
	
	/**
	 * Manager for the C dimension
	 */
	private class DimensionManagerC extends DimensionManager
	{
		private int _step;
		
		public DimensionManagerC(int order, int step)
		{
			super(order, _sizeC);
			_step = step;
		}
		
		@Override
		protected void increaseValue()
		{
			_posC += _step;
			notifyPosCChanged();
		}
		
		@Override
		protected void resetValue()
		{
			_posC = 0;
			notifyPosCChanged();
		}

		@Override
		protected int getNextValue()
		{
			return _posC+_step;
		}
	}
}
