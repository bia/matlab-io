package plugins.ylemontag.matlabio.lib;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Base class used to represent a Matlab data element
 */
public class MLObject
{
	private MLMeta _meta;
	
	/**
	 * Constructor
	 */
	protected MLObject(MLMeta meta)
	{
		_meta = meta;
	}
	
	/**
	 * All the meta-data associated to the object
	 */
	public MLMeta getMeta()
	{
		return _meta;
	}
	
	/**
	 * Data element type
	 */
	public MLType getType()
	{
		return _meta.getType();
	}
	
	/**
	 * Name of the data element type
	 */
	public String getName()
	{
		return _meta.getName();
	}
	
	/**
	 * Dimensions of the data element
	 */
	public int[] getDimensions()
	{
		return _meta.getDimensions();
	}
	
	/**
	 * Number of components, i.e. product of all dimensions
	 */
	public int getSize()
	{
		return _meta.getSize();
	}
	
	/**
	 * Whether the underlying object contains complex data or not
	 * @warning This make sense only in the case of numeric or sparse class.
	 *          Otherwise, the function returns false.
	 */
	public boolean getIsComplex()
	{
		return _meta.getIsComplex();
	}
}
