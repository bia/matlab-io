package plugins.ylemontag.matlabio.lib;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Base class used to represent a Matlab data element, whose supports random access
 */
public abstract class MLArray extends MLObject
{	
	/**
	 * Constructor
	 */
	protected MLArray(MLMeta meta)
	{
		super(meta);
	}
	
	/**
	 * Try to return the content as a string
	 */
	public String getAsString() throws MLIOException
	{
		char[] buffer = getAsCharArray();
		String retVal = "";
		for(char c : buffer) {
			retVal += c;
		}
		return retVal;
	}
	
	/**
	 * Try to return the content as a char-valued scalar
	 */
	public boolean getAsLogical() throws MLIOException { ensureIsScalar(); return getAsLogicalArray()[0]; }
	
	/**
	 * Try to return the content as a char-valued scalar
	 */
	public char getAsChar() throws MLIOException { ensureIsScalar(); return getAsCharArray()[0]; }
	
	/**
	 * Try to return the content as a double-valued scalar
	 */
	public double getAsDouble() throws MLIOException { ensureIsScalar(); return getAsDoubleArray()[0]; }
	
	/**
	 * Try to return the content as a single-valued scalar
	 */
	public float getAsSingle() throws MLIOException { ensureIsScalar(); return getAsSingleArray()[0]; }
	
	/**
	 * Try to return the content as an int8-valued scalar
	 */
	public byte getAsInt8() throws MLIOException { ensureIsScalar(); return getAsInt8Array()[0]; }
	
	/**
	 * Try to return the content as an int16-valued scalar
	 */
	public short getAsInt16() throws MLIOException { ensureIsScalar(); return getAsInt16Array()[0]; }
	
	/**
	 * Try to return the content as an int32-valued scalar
	 */
	public int getAsInt32() throws MLIOException { ensureIsScalar(); return getAsInt32Array()[0]; }
	
	/**
	 * Try to return the content as an uint64-valued scalar
	 */
	public long getAsInt64() throws MLIOException { ensureIsScalar(); return getAsInt64Array()[0]; }
	
	/**
	 * Try to return the content as an uint8-valued scalar
	 */
	public byte getAsUInt8() throws MLIOException { ensureIsScalar(); return getAsUInt8Array()[0]; }
	
	/**
	 * Try to return the content as an uint16-valued scalar
	 */
	public short getAsUInt16() throws MLIOException { ensureIsScalar(); return getAsUInt16Array()[0]; }
	
	/**
	 * Try to return the content as an uint32-valued scalar
	 */
	public int getAsUInt32() throws MLIOException { ensureIsScalar(); return getAsUInt32Array()[0]; }
	
	/**
	 * Try to return the content as an uint64-valued scalar
	 */
	public long getAsUInt64() throws MLIOException { ensureIsScalar(); return getAsUInt64Array()[0]; }
	
	/**
	 * Try to return the content as a boolean-valued array
	 */
	public boolean[] getAsLogicalArray() throws MLIOException { throwBadCastException(MLType.LOGICAL); return null; }
	
	/**
	 * Try to return the content as a char-valued array
	 */
	public char[] getAsCharArray() throws MLIOException { throwBadCastException(MLType.CHAR); return null; }
	
	/**
	 * Try to return the content as a double-valued array
	 */
	public double[] getAsDoubleArray() throws MLIOException { throwBadCastException(MLType.DOUBLE); return null; }
	
	/**
	 * Try to return the content as a single-valued array
	 */
	public float[] getAsSingleArray() throws MLIOException { throwBadCastException(MLType.SINGLE); return null; }
	
	/**
	 * Try to return the content as an int8-valued array
	 */
	public byte[] getAsInt8Array() throws MLIOException { throwBadCastException(MLType.INT8); return null; }
	
	/**
	 * Try to return the content as an int16-valued array
	 */
	public short[] getAsInt16Array() throws MLIOException { throwBadCastException(MLType.INT16); return null; }
	
	/**
	 * Try to return the content as an int32-valued array
	 */
	public int[] getAsInt32Array() throws MLIOException { throwBadCastException(MLType.INT32); return null; }
	
	/**
	 * Try to return the content as an int64-valued array
	 */
	public long[] getAsInt64Array() throws MLIOException { throwBadCastException(MLType.INT64); return null; }
	
	/**
	 * Try to return the content as an uint8-valued array
	 */
	public byte[] getAsUInt8Array() throws MLIOException { throwBadCastException(MLType.UINT8); return null; }
	
	/**
	 * Try to return the content as an uint16-valued array
	 */
	public short[] getAsUInt16Array() throws MLIOException { throwBadCastException(MLType.UINT16); return null; }
	
	/**
	 * Try to return the content as an uint32-valued array
	 */
	public int[] getAsUInt32Array() throws MLIOException { throwBadCastException(MLType.UINT32); return null; }
	
	/**
	 * Try to return the content as an uint64-valued array
	 */
	public long[] getAsUInt64Array() throws MLIOException { throwBadCastException(MLType.UINT64); return null; }
	
	/**
	 * Exception thrown when using a non-implemented getAsTargetTypeArray() function
	 */
	private void throwBadCastException(MLType targetType) throws MLIOException
	{
		throw new MLIOException("The current MLArray is not " + targetType + "-valued.");
	}
	
	/**
	 * Ensure that the current MLArray object wrap a scalar value
	 */
	private void ensureIsScalar() throws MLIOException
	{
		if(getSize()!=1) {
			throw new MLIOException("The current MLArray is not a scalar.");
		}
	}
}
