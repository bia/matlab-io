package plugins.ylemontag.matlabio.lib;

/**
 * 
 * @author Yoann Le Montagner
 *
 * Specialized classes implementing different types of MLArray
 */
public class MLArrays
{
	/**
	 * Matlab array with boolean values
	 */
	public static class Logical extends MLArray
	{
		private boolean[] _buffer;
		
		/**
		 * Return the underlying array
		 */
		public boolean[] get()
		{
			return _buffer;
		}
		
		/**
		 * Constructor
		 */
		public Logical(MLMeta meta)
		{
			super(meta);
			_buffer = new boolean[meta.getSize()];
		}
		
		@Override
		public boolean[] getAsLogicalArray() throws MLIOException
		{
			return _buffer;
		}
		
		/**
		 * Construct a boolean array from a scalar boolean value
		 */
		public Logical(String name, boolean value) throws MLIOException
		{
			this(name, new boolean[] {value});
		}
		
		/**
		 * Wrap an existing boolean array into a MLArray structure
		 */
		public Logical(String name, boolean[] data) throws MLIOException
		{
			super(new MLMeta(MLType.LOGICAL, name, new int[] {data.length}, false));
			_buffer = data;
		}
	}
	
	/**
	 * Matlab array with text character values
	 */
	public static class Char extends MLArray
	{
		private char[] _buffer;
		
		/**
		 * Return the underlying array
		 */
		public char[] get()
		{
			return _buffer;
		}
		
		/**
		 * Constructor
		 */
		public Char(MLMeta meta)
		{
			super(meta);
			_buffer = new char[meta.getSize()];
		}
		
		@Override
		public char[] getAsCharArray() throws MLIOException
		{
			return _buffer;
		}
		
		/**
		 * Construct a char array from a string
		 */
		public Char(String name, String value) throws MLIOException
		{
			super(new MLMeta(MLType.CHAR, name, new int[] {1, value.length()}, false));
			_buffer = new char[value.length()];
			for(int k=0; k<value.length(); ++k) {
				_buffer[k] = value.charAt(k);
			}
		}
		
		/**
		 * Construct a char array from a scalar char value
		 */
		public Char(String name, char value) throws MLIOException
		{
			this(name, new char[] {value});
		}
		
		/**
		 * Wrap an existing char array into a MLArray structure
		 */
		public Char(String name, char[] data) throws MLIOException
		{
			super(new MLMeta(MLType.CHAR, name, new int[] {data.length}, false));
			_buffer = data;
		}
	}
	
	/**
	 * Matlab numeric array with double values
	 */
	public static class Double extends Numeric<double[]>
	{
		public Double(String name, double value) throws MLIOException { this(name, new double[] {value}); }
		public Double(String name, double[] data) throws MLIOException { super(MLType.DOUBLE, name, data.length, data); }
		public Double(MLMeta meta) { super(meta); }
		
		@Override
		public double[] getAsDoubleArray() throws MLIOException { return getAsNativeArray(); }
		
		@Override
		protected double[] allocate(int size) { return new double[size]; }
	}
	
	/**
	 * Matlab numeric array with float values
	 */
	public static class Single extends Numeric<float[]>
	{
		public Single(String name, float value) throws MLIOException { this(name, new float[] {value}); }
		public Single(String name, float[] data) throws MLIOException { super(MLType.SINGLE, name, data.length, data); }
		public Single(MLMeta meta) { super(meta); }
		
		@Override
		public float[] getAsSingleArray() throws MLIOException { return getAsNativeArray(); }
		
		@Override
		protected float[] allocate(int size) { return new float[size]; }
	}
	
	/**
	 * Matlab numeric array with signed 8 bits values
	 */
	public static class Int8 extends Numeric<byte[]>
	{
		public Int8(String name, byte value) throws MLIOException { this(name, new byte[] {value}); }
		public Int8(String name, byte[] data) throws MLIOException { super(MLType.INT8, name, data.length, data); }
		public Int8(MLMeta meta) { super(meta); }
		
		@Override
		public byte[] getAsInt8Array() throws MLIOException { return getAsNativeArray(); }
		
		@Override
		protected byte[] allocate(int size) { return new byte[size]; }
	}
	
	/**
	 * Matlab numeric array with signed 16 bits values
	 */
	public static class Int16 extends Numeric<short[]>
	{
		public Int16(String name, short value) throws MLIOException { this(name, new short[] {value}); }
		public Int16(String name, short[] data) throws MLIOException { super(MLType.INT16, name, data.length, data); }
		public Int16(MLMeta meta) { super(meta); }
		
		@Override
		public short[] getAsInt16Array() throws MLIOException { return getAsNativeArray(); }
		
		@Override
		protected short[] allocate(int size) { return new short[size]; }
	}
	
	/**
	 * Matlab numeric array with signed 32 bits values
	 */
	public static class Int32 extends Numeric<int[]>
	{
		public Int32(String name, int value) throws MLIOException { this(name, new int[] {value}); }
		public Int32(String name, int[] data) throws MLIOException { super(MLType.INT32, name, data.length, data); }
		public Int32(MLMeta meta) { super(meta); }
		
		@Override
		public int[] getAsInt32Array() throws MLIOException { return getAsNativeArray(); }
		
		@Override
		protected int[] allocate(int size) { return new int[size]; }
	}
	
	/**
	 * Matlab numeric array with signed 64 bits values
	 */
	public static class Int64 extends Numeric<long[]>
	{
		public Int64(String name, long value) throws MLIOException { this(name, new long[] {value}); }
		public Int64(String name, long[] data) throws MLIOException { super(MLType.INT64, name, data.length, data); }
		public Int64(MLMeta meta) { super(meta); }
		
		@Override
		public long[] getAsInt64Array() throws MLIOException { return getAsNativeArray(); }
		
		@Override
		protected long[] allocate(int size) { return new long[size]; }
	}
	
	/**
	 * Matlab numeric array with unsigned 8 bits values
	 */
	public static class UInt8 extends Numeric<byte[]>
	{
		public UInt8(String name, byte value) throws MLIOException { this(name, new byte[] {value}); }
		public UInt8(String name, byte[] data) throws MLIOException { super(MLType.UINT8, name, data.length, data); }
		public UInt8(MLMeta meta) { super(meta); }
		
		@Override
		public byte[] getAsUInt8Array() throws MLIOException { return getAsNativeArray(); }
		
		@Override
		protected byte[] allocate(int size) { return new byte[size]; }
	}
	
	/**
	 * Matlab numeric array with unsigned 16 bits values
	 */
	public static class UInt16 extends Numeric<short[]>
	{
		public UInt16(String name, short value) throws MLIOException { this(name, new short[] {value}); }
		public UInt16(String name, short[] data) throws MLIOException { super(MLType.UINT16, name, data.length, data); }
		public UInt16(MLMeta meta) { super(meta); }
		
		@Override
		public short[] getAsUInt16Array() throws MLIOException { return getAsNativeArray(); }
		
		@Override
		protected short[] allocate(int size) { return new short[size]; }
	}
	
	/**
	 * Matlab numeric array with unsigned 32 bits values
	 */
	public static class UInt32 extends Numeric<int[]>
	{
		public UInt32(String name, int value) throws MLIOException { this(name, new int[] {value}); }
		public UInt32(String name, int[] data) throws MLIOException { super(MLType.UINT32, name, data.length, data); }
		public UInt32(MLMeta meta) { super(meta); }
		
		@Override
		public int[] getAsUInt32Array() throws MLIOException { return getAsNativeArray(); }
		
		@Override
		protected int[] allocate(int size) { return new int[size]; }
	}
	
	/**
	 * Matlab numeric array with unsigned 64 bits values
	 */
	public static class UInt64 extends Numeric<long[]>
	{
		public UInt64(String name, long value) throws MLIOException { this(name, new long[] {value}); }
		public UInt64(String name, long[] data) throws MLIOException { super(MLType.UINT64, name, data.length, data); }
		public UInt64(MLMeta meta) { super(meta); }
		
		@Override
		public long[] getAsUInt64Array() throws MLIOException { return getAsNativeArray(); }
		
		@Override
		protected long[] allocate(int size) { return new long[size]; }
	}
	
	/**
	 * Base class for numeric arrays
	 */
	static abstract class Numeric<T> extends MLArray
	{
		private T _real     ;
		private T _imaginary;
		
		/**
		 * Wrap an existing array into a MLArray structure
		 */
		protected Numeric(MLType type, String name, int length, T data) throws MLIOException
		{
			super(new MLMeta(type, name, new int[] {length}, false));
			_real = data;
		}
		
		/**
		 * Constructor
		 */
		protected Numeric(MLMeta meta)
		{
			super(meta);
			_real = allocate(meta.getSize());
			if(getIsComplex()) {
				_imaginary = allocate(meta.getSize());
			}
		}
		
		/**
		 * Real components of the Matlab array
		 */
		public T getReal()
		{
			return _real;
		}
		
		/**
		 * Imaginary components of the Matlab array
		 */
		public T getImaginary()
		{
			return _imaginary;
		}
		
		/**
		 * Function used to allocate an array of the right size
		 */
		protected abstract T allocate(int size);
		
		/**
		 * Function to call to implement the getAsSomethingArray() function
		 * in the derived classes
		 */
		protected T getAsNativeArray() throws MLIOException
		{
			if(getIsComplex()) {
				throw new MLIOException("The current MLArray is complex-valued.");
			}
			return _real;
		}
	}
}
