package plugins.ylemontag.matlabio.lib;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Type of a Matlab object (correspond to constants mxFOO_CLASS in the Matlab .mat file reference)
 */
public enum MLType
{	
	CELL   ((byte) 1, false, null            , "cell"   ),
	STRUCT ((byte) 2, false, null            , "struct" ),
	OBJECT ((byte) 3, false, null            , "object" ),
	CHAR   ((byte) 4, false, MLRawType.UTF16 , "char"   ),
	SPARSE ((byte) 5, false, null            , "sparse" ),
	DOUBLE ((byte) 6, true , MLRawType.DOUBLE, "double" ),
	SINGLE ((byte) 7, true , MLRawType.SINGLE, "single" ),
	INT8   ((byte) 8, true , MLRawType.INT8  , "int8"   ),
	UINT8  ((byte) 9, true , MLRawType.UINT8 , "uint8"  ),
	INT16  ((byte)10, true , MLRawType.INT16 , "int16"  ),
	UINT16 ((byte)11, true , MLRawType.UINT16, "uint16" ),
	INT32  ((byte)12, true , MLRawType.INT32 , "int32"  ),
	UINT32 ((byte)13, true , MLRawType.UINT32, "uint32" ),
	INT64  ((byte)14, true , MLRawType.INT64 , "int64"  ),
	UINT64 ((byte)15, true , MLRawType.UINT64, "uint64" ),
	LOGICAL((byte) 9, false, MLRawType.UINT8 , "logical"); // Not in the Matlab specs
	
	private byte      _code     ;
	private boolean   _isNumeric; // Public
	private String    _name     ; // Public
	private MLRawType _rawType  ;
	
	/**
	 * Convert a numeric value read from a file into a MLClass object
	 */
	static MLType byteToMLType(byte value) throws MLIOException
	{
		switch(value) {
			case  1: return CELL  ;
			case  2: return STRUCT;
			case  3: return OBJECT;
			case  4: return CHAR  ;
			case  5: return SPARSE;
			case  6: return DOUBLE;
			case  7: return SINGLE;
			case  8: return INT8  ;
			case  9: return UINT8 ;
			case 10: return INT16 ;
			case 11: return UINT16;
			case 12: return INT32 ;
			case 13: return UINT32;
			case 14: return INT64 ;
			case 15: return UINT64;
			// Never return LOGICAL as this type is not standard
			default:
				throw new MLIOException("Unknown MLType code: " + value);
		}
	}
	
	/**
	 * Constructor
	 */
	private MLType(byte code, boolean isNumeric, MLRawType rawType, String name)
	{
		_code      = code     ;
		_isNumeric = isNumeric;
		_name      = name     ;
		_rawType   = rawType  ;
	}
	
	/**
	 * Code used to flag the datatype in Matlab .mat files
	 * @remarks Return the code corresponding to MLType.UINT8 in case of MLType.LOGICAL
	 */
	byte getCode()
	{
		return _code;
	}
	
	/**
	 * Check whether this class is a numeric class
	 */
	public boolean getIsNumeric()
	{
		return _isNumeric;
	}
	
	/**
	 * Canonical raw type corresponding to the current type
	 * @warning Throw an exception for non-numeric types
	 */
	MLRawType getRawType() throws MLIOException
	{
		if(_rawType==null) {
			throw new MLIOException("Cannot associate a raw type to type " + _name);
		}
		return _rawType;
	}
	
	@Override
	public String toString()
	{
		return _name;
	}
}
