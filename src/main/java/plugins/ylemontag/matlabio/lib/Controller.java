package plugins.ylemontag.matlabio.lib;

import java.util.LinkedList;


/**
 * 
 * @author Yoann Le Montagner
 *
 * Object used to control and interrupt a potentially long operation on a Matlab .mat file
 */
public class Controller
{	
	/**
	 * Exception thrown when an operation interruption have been requested
	 * (for instance by the user)
	 */
	public static class CanceledByUser extends Exception
	{
		private static final long serialVersionUID = 1L;
	}
	
	/**
	 * Stack item used to recursively keep track of the task progress
	 */
	private static class StackItem
	{
		double progressAtBegin;
		double atomicProgress ;
		int    currentPosition;
		int    maximumPosition;
	}
	
	private boolean _cancelFlag;
	private double _currentProgress;
	private StackItem _currentOp;
	private LinkedList<StackItem> _stack;
	
	/**
	 * Constructor
	 */
	public Controller()
	{
		_cancelFlag = false;
		_currentProgress = 0.0;
		_currentOp = new StackItem();
		_currentOp.progressAtBegin = 0.0;
		_currentOp.atomicProgress  = 1.0;
		_currentOp.currentPosition = 0;
		_currentOp.maximumPosition = 1;
		_stack = new LinkedList<Controller.StackItem>();
	}
	
	/**
	 * Method to call to ask the corresponding operation to stop as soon as possible
	 * @warning Calling this method does not mean the computation will actually
	 *          be interrupted. The operation is interrupted if a CanceledByUser
	 *          exception is thrown.
	 */
	public void cancelComputation()
	{
		_cancelFlag = true;
	}
	
	/**
	 * Retrieve the current progress
	 */
	public double getCurrentProgress()
	{
		return _currentProgress;
	}
	
	/**
	 * Check-point function called by the thread
	 */
	public void checkPoint() throws CanceledByUser
	{
		if(_cancelFlag) {
			throw new CanceledByUser();
		}
	}
	
	/**
	 * Start a new check-point counter
	 */
	public void startCounter(int nbSteps)
	{
		nbSteps = Math.max(1, nbSteps);
		double newAtomicProgress = _currentOp.currentPosition<_currentOp.maximumPosition ?
			_currentOp.atomicProgress / nbSteps : 0.0;
		StackItem newOp = new StackItem();
		newOp.progressAtBegin = _currentProgress;
		newOp.atomicProgress  = newAtomicProgress;
		newOp.currentPosition = 0;
		newOp.maximumPosition = nbSteps;
		_stack.push(_currentOp);
		_currentOp = newOp;
	}
	
	/**
	 * Check-point function called by the thread (updating the position)
	 */
	public void checkPointNext() throws CanceledByUser
	{
		increaseCurrentStepCounter(1);
		checkPoint();
	}
	
	/**
	 * Check-point function called by the thread (updating the position)
	 */
	public void checkPointDelta(int delta) throws CanceledByUser
	{
		increaseCurrentStepCounter(delta);
		checkPoint();
	}
	
	/**
	 * End the currently-running check-point counter
	 */
	public void stopCounter()
	{
		_currentOp = _stack.pop();
		increaseCurrentStepCounter(1); 
	}
	
	/**
	 * Increase the current step counter, and refresh the overall progress value
	 */
	private void increaseCurrentStepCounter(int delta)
	{
		_currentOp.currentPosition = Math.min(_currentOp.currentPosition+delta, _currentOp.maximumPosition);
		_currentProgress = _currentOp.progressAtBegin + _currentOp.atomicProgress*_currentOp.currentPosition;
	}
}
