package plugins.ylemontag.matlabio.lib;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteOrder;

/**
 * 
 * @author Yoann Le Montagner
 *
 * Specialized classes implementing different types of MLOStream
 */
public class MLOStreams
{
	/**
	 * Matlab numeric output stream with double values
	 */
	public static class Double extends Numeric
	{
		/**
		 * Write the next value
		 */
		public void put(double value) throws IOException
		{
			_currentValue = value;
			writeNextValue();
		}
		
		private double _currentValue;
		
		Double(MLMeta meta, OutputStream target, ByteOrder endianness) throws IOException
		{
			super(meta, target, endianness);
			_currentValue = 0.0;
		}
		
		@Override
		protected void saveNextValue(MLDataOutputStream stream) throws IOException
		{
			stream.pushDouble(_currentValue);
		}
	}
	
	/**
	 * Matlab numeric output stream with float values
	 */
	public static class Single extends Numeric
	{
		/**
		 * Write the next value
		 */
		public void put(float value) throws IOException
		{
			_currentValue = value;
			writeNextValue();
		}
		
		private float _currentValue;
		
		Single(MLMeta meta, OutputStream target, ByteOrder endianness) throws IOException
		{
			super(meta, target, endianness);
			_currentValue = 0.0f;
		}
		
		@Override
		protected void saveNextValue(MLDataOutputStream stream) throws IOException
		{
			stream.pushSingle(_currentValue);
		}
	}
	
	/**
	 * Matlab numeric output stream with signed 8 bits values
	 */
	public static class Int8 extends Numeric
	{
		/**
		 * Write the next value
		 */
		public void put(byte value) throws IOException
		{
			_currentValue = value;
			writeNextValue();
		}
		
		private byte _currentValue;
		
		Int8(MLMeta meta, OutputStream target, ByteOrder endianness) throws IOException
		{
			super(meta, target, endianness);
			_currentValue = 0;
		}
		
		@Override
		protected void saveNextValue(MLDataOutputStream stream) throws IOException
		{
			stream.pushInt8(_currentValue);
		}
	}
	
	/**
	 * Matlab numeric output stream with signed 16 bits values
	 */
	public static class Int16 extends Numeric
	{
		/**
		 * Write the next value
		 */
		public void put(short value) throws IOException
		{
			_currentValue = value;
			writeNextValue();
		}
		
		private short _currentValue;
		
		Int16(MLMeta meta, OutputStream target, ByteOrder endianness) throws IOException
		{
			super(meta, target, endianness);
			_currentValue = 0;
		}
		
		@Override
		protected void saveNextValue(MLDataOutputStream stream) throws IOException
		{
			stream.pushInt16(_currentValue);
		}
	}
	
	/**
	 * Matlab numeric output stream with signed 32 bits values
	 */
	public static class Int32 extends Numeric
	{
		/**
		 * Write the next value
		 */
		public void put(int value) throws IOException
		{
			_currentValue = value;
			writeNextValue();
		}
		
		private int _currentValue;
		
		Int32(MLMeta meta, OutputStream target, ByteOrder endianness) throws IOException
		{
			super(meta, target, endianness);
			_currentValue = 0;
		}
		
		@Override
		protected void saveNextValue(MLDataOutputStream stream) throws IOException
		{
			stream.pushInt32(_currentValue);
		}
	}
	
	/**
	 * Matlab numeric output stream with signed 64 bits values
	 */
	public static class Int64 extends Numeric
	{
		/**
		 * Write the next value
		 */
		public void put(long value) throws IOException
		{
			_currentValue = value;
			writeNextValue();
		}
		
		private long _currentValue;
		
		Int64(MLMeta meta, OutputStream target, ByteOrder endianness) throws IOException
		{
			super(meta, target, endianness);
			_currentValue = 0;
		}
		
		@Override
		protected void saveNextValue(MLDataOutputStream stream) throws IOException
		{
			stream.pushInt64(_currentValue);
		}
	}
	
	/**
	 * Matlab numeric output stream with unsigned 8 bits values
	 */
	public static class UInt8 extends Numeric
	{
		/**
		 * Write the next value
		 */
		public void put(byte value) throws IOException
		{
			_currentValue = value;
			writeNextValue();
		}
		
		private byte _currentValue;
		
		UInt8(MLMeta meta, OutputStream target, ByteOrder endianness) throws IOException
		{
			super(meta, target, endianness);
			_currentValue = 0;
		}
		
		@Override
		protected void saveNextValue(MLDataOutputStream stream) throws IOException
		{
			stream.pushUInt8(_currentValue);
		}
	}
	
	/**
	 * Matlab numeric output stream with unsigned 16 bits values
	 */
	public static class UInt16 extends Numeric
	{
		/**
		 * Write the next value
		 */
		public void put(short value) throws IOException
		{
			_currentValue = value;
			writeNextValue();
		}
		
		private short _currentValue;
		
		UInt16(MLMeta meta, OutputStream target, ByteOrder endianness) throws IOException
		{
			super(meta, target, endianness);
			_currentValue = 0;
		}
		
		@Override
		protected void saveNextValue(MLDataOutputStream stream) throws IOException
		{
			stream.pushUInt16(_currentValue);
		}
	}
	
	/**
	 * Matlab numeric output stream with unsigned 32 bits values
	 */
	public static class UInt32 extends Numeric
	{
		/**
		 * Write the next value
		 */
		public void put(int value) throws IOException
		{
			_currentValue = value;
			writeNextValue();
		}
		
		private int _currentValue;
		
		UInt32(MLMeta meta, OutputStream target, ByteOrder endianness) throws IOException
		{
			super(meta, target, endianness);
			_currentValue = 0;
		}
		
		@Override
		protected void saveNextValue(MLDataOutputStream stream) throws IOException
		{
			stream.pushUInt32(_currentValue);
		}
	}
	
	/**
	 * Matlab numeric output stream with unsigned 64 bits values
	 */
	public static class UInt64 extends Numeric
	{
		/**
		 * Write the next value
		 */
		public void put(long value) throws IOException
		{
			_currentValue = value;
			writeNextValue();
		}
		
		private long _currentValue;
		
		UInt64(MLMeta meta, OutputStream target, ByteOrder endianness) throws IOException
		{
			super(meta, target, endianness);
			_currentValue = 0;
		}
		
		@Override
		protected void saveNextValue(MLDataOutputStream stream) throws IOException
		{
			stream.pushUInt64(_currentValue);
		}
	}
	
	/** 
	 * Matlab numeric output stream
	 */
	static abstract class Numeric extends MLOStream
	{
		/**
		 * Current section in the stream
		 */
		private static enum Section
		{
			REAL     ,
			IMAGINARY,
			EOS      ;
		}
		
		private OutputStream       _target        ;
		private ByteOrder          _endianness    ;
		private Section            _currentSection;
		private MLDataOutputStream _dataStream    ;
		private int                _nextIndex     ;
		
		/**
		 * Constructor
		 */
		protected Numeric(MLMeta meta, OutputStream target, ByteOrder endianness) throws IOException
		{
			super(meta);
			_target = target;
			_endianness = endianness;
			prepareNextSection();
		}
		
		/**
		 * Check whether the end of both the real and the imaginary streams has been reached
		 */
		public boolean isAtEnd()
		{
			return _currentSection==Section.EOS;
		}
		
		/**
		 * Return true if the the cursor is not at the end of the stream and if the next
		 * value to be read belongs to the real data stream
		 */
		public boolean isRealPart()
		{
			return _currentSection==Section.REAL;
		}
		
		/**
		 * Return true if the the cursor is not at the end of the stream and if the next
		 * value to be read belongs to the real data stream
		 */
		public boolean isImaginaryPart()
		{
			return _currentSection==Section.IMAGINARY;
		}
		
		/**
		 * The derived class should push the previously saved data element in the stream
		 * when this function is called
		 */
		protected abstract void saveNextValue(MLDataOutputStream stream) throws IOException;
		
		/**
		 * The derived class should call this method at each write request. This method
		 * will call 'saveNextValue' with the correct parameter values
		 */
		protected void writeNextValue() throws IOException
		{
			if(_dataStream==null) {
				throw new MLUtil.EndOfStream();
			}
			saveNextValue(_dataStream);
			++_nextIndex;
			if(_dataStream.isAtEnd()) {
				prepareNextSection();
			}
		}
		
		/**
		 * Create the data stream corresponding to the next data section (either the
		 * real part or the imaginary part)
		 */
		private void prepareNextSection() throws IOException
		{
			// Update the section flag
			if(_currentSection==null) {
				_currentSection = Section.REAL;
			}
			else if(_currentSection==Section.REAL && getIsComplex()) {
				_currentSection = Section.IMAGINARY;
			}
			else {
				_currentSection = Section.EOS;
				_dataStream = null;
				_target.close();
				return;
			}
			
			// Prepare the next section
			_dataStream = new MLDataOutputStream(_target, _endianness, getType(), getSize());
			_nextIndex = 0;
			if(_dataStream.isAtEnd()) {
				_currentSection = Section.EOS;
				_dataStream = null;
				_target.close();
			}
		}
		
		@Override
		public void skip(int num) throws IOException
		{
			if(_dataStream==null) {
				throw new MLUtil.EndOfStream();
			}
			int remainingElementsInCurrentSection = getSize()-_nextIndex;
			if(num < remainingElementsInCurrentSection) {
				_nextIndex += num;
				_dataStream.skip(num);
			}
			else {
				_dataStream.skip(remainingElementsInCurrentSection);
				prepareNextSection();
				_nextIndex = num - remainingElementsInCurrentSection;
				_dataStream.skip(_nextIndex);
			}
		}
		
		@Override
		public void skip(int num, Controller controller) throws IOException, Controller.CanceledByUser
		{
			if(_dataStream==null) {
				throw new MLUtil.EndOfStream();
			}
			int remainingElementsInCurrentSection = getSize()-_nextIndex;
			if(num < remainingElementsInCurrentSection) {
				_nextIndex += num;
				_dataStream.skip(num, controller);
			}
			else {
				_dataStream.skip(remainingElementsInCurrentSection, controller);
				prepareNextSection();
				_nextIndex = num - remainingElementsInCurrentSection;
				_dataStream.skip(_nextIndex, controller);
			}
		}
	}
}
