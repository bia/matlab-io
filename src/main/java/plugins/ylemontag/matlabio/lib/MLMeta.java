package plugins.ylemontag.matlabio.lib;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Object containing all meta-informations relative to a Matlab data element
 */
public class MLMeta
{
	private MLType _type;
	private String _name;
	private int[] _dimensions;
	private int _size;
	private boolean _isComplex;
	
	/**
	 * Constructor
	 */
	public MLMeta(MLType type, String name, int[] dimensions, boolean isComplex) throws MLIOException
	{
		_type = type;
		_name = name;
		_isComplex = isComplex;
		if(!isValidName(_name)) {
			throw new MLIOException("Invalid name for a Matlab object: " + _name);
		}
		if(_isComplex && !(_type.getIsNumeric() || _type==MLType.SPARSE)) {
			throw new MLIOException("Non-numeric objects must have a complex flag set to false");
		}
		_dimensions = dimensions;
		_size = 1;
		for(int d : _dimensions) {
			_size *= d;
		}
	}
	
	/**
	 * Data element type
	 */
	public MLType getType()
	{
		return _type;
	}
	
	/**
	 * Name of the data element type
	 */
	public String getName()
	{
		return _name;
	}
	
	/**
	 * Dimensions of the data element
	 */
	public int[] getDimensions()
	{
		return _dimensions;
	}
	
	/**
	 * Dimensions of the data element as a human readable string 
	 */
	public String getDimensionsAsString()
	{
		int nDims = _dimensions.length;
		String retVal = "";
		for(int k=0; k<nDims; ++k) {
			if(k!=0) {
				retVal +="\u00d7";
			}
			retVal += _dimensions[k];
		}
		return retVal;
	}
	
	/**
	 * Number of components, i.e. product of all dimensions
	 */
	public int getSize()
	{
		return _size;
	}
	
	/**
	 * Whether the underlying object contains complex data or not
	 * @warning This make sense only in the case of numeric or sparse class.
	 *          Otherwise, the function returns false.
	 */
	public boolean getIsComplex()
	{
		return _isComplex;
	}
	
	/**
	 * Check whether a string is a valid Matlab variable name
	 */
	public static boolean isValidName(String name)
	{
		int length = name.length();
		if(length==0) {
			return false;
		}
		for(int k=0; k<length; ++k) {
			char currentChar = name.charAt(k);
			if(!(
				(currentChar>='A' && currentChar<='Z') ||
				(currentChar>='a' && currentChar<='z') ||
				(currentChar>='0' && currentChar<='9' && k!=0) || currentChar=='_'
			)) {
				return false;
			}
		}
		return true;
	}
}
