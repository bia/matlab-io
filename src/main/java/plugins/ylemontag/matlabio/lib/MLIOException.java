package plugins.ylemontag.matlabio.lib;

import java.io.IOException;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Exception thrown by the Matlab IO library
 */
public class MLIOException extends IOException
{
	private static final long serialVersionUID = 1L;

	/**
	 * Default constructor
	 */
	public MLIOException()
	{
		super();
	}
	
	/**
	 * Constructor
	 */
	public MLIOException(String message)
	{
		super(message);
	}
}
