package plugins.ylemontag.matlabio.lib;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.IllegalCharsetNameException;

import plugins.ylemontag.matlabio.lib.Controller.CanceledByUser;

/**
 * 
 * @author Yoann Le Montagner
 *
 * Wrap an input stream and perform type conversion on the fly if necessary
 */
class MLDataInputStream
{
	/**
	 * Maximum size of the buffer
	 */
	private static final int CHUNK_SIZE = 0x1000;
	
	private InputStream _stream;
	private ByteOrder _endianness;
	private MLType _targetType;
	private MLUtil.FieldHeader _header;
	private Converter _converter;
	private int _remains;
	private ByteBuffer _buffer;
	
	/**
	 * Constructor
	 */
	MLDataInputStream(InputStream stream, ByteOrder endianness, MLType targetType)
		throws IOException
	{
		_stream = stream;
		_endianness = endianness;
		_targetType = targetType;
		_header = MLUtil.readFieldHeader(_stream, _endianness);
		_converter = buildConverter(_targetType, _header.getType());
		_remains = _header.getDataLength();
		loadNextBuffer();
	}
	
	/**
	 * Check whether the end of the stream has been reached
	 */
	boolean isAtEnd()
	{
		return _remains==0 && _buffer.remaining()==0;
	}
	
	/**
	 * Skip a given number of elements
	 */
	void skip(int elements) throws IOException
	{
		switch(_targetType) {
			case LOGICAL: for(int k=0; k<elements; ++k) { consumeLogical(); } break;
			case CHAR   : for(int k=0; k<elements; ++k) { consumeChar   (); } break;
			case DOUBLE : for(int k=0; k<elements; ++k) { consumeDouble (); } break;
			case SINGLE : for(int k=0; k<elements; ++k) { consumeSingle (); } break;
			case INT8   : for(int k=0; k<elements; ++k) { consumeInt8   (); } break;
			case INT16  : for(int k=0; k<elements; ++k) { consumeInt16  (); } break;
			case INT32  : for(int k=0; k<elements; ++k) { consumeInt32  (); } break;
			case INT64  : for(int k=0; k<elements; ++k) { consumeInt64  (); } break;
			case UINT8  : for(int k=0; k<elements; ++k) { consumeUInt8  (); } break;
			case UINT16 : for(int k=0; k<elements; ++k) { consumeUInt16 (); } break;
			case UINT32 : for(int k=0; k<elements; ++k) { consumeUInt32 (); } break;
			case UINT64 : for(int k=0; k<elements; ++k) { consumeUInt64 (); } break;
			default:
				throw new MLIOException("Cannot skip in a stream of " + _targetType + " elements");
		}
	}
	
	/**
	 * Skip a given number of elements
	 */
	void skip(int elements, Controller controller) throws IOException, Controller.CanceledByUser
	{
		switch(_targetType) {
			case LOGICAL: for(int k=0; k<elements; ++k) { controller.checkPointNext(); consumeLogical(); } break;
			case CHAR   : for(int k=0; k<elements; ++k) { controller.checkPointNext(); consumeChar   (); } break;
			case DOUBLE : for(int k=0; k<elements; ++k) { controller.checkPointNext(); consumeDouble (); } break;
			case SINGLE : for(int k=0; k<elements; ++k) { controller.checkPointNext(); consumeSingle (); } break;
			case INT8   : for(int k=0; k<elements; ++k) { controller.checkPointNext(); consumeInt8   (); } break;
			case INT16  : for(int k=0; k<elements; ++k) { controller.checkPointNext(); consumeInt16  (); } break;
			case INT32  : for(int k=0; k<elements; ++k) { controller.checkPointNext(); consumeInt32  (); } break;
			case INT64  : for(int k=0; k<elements; ++k) { controller.checkPointNext(); consumeInt64  (); } break;
			case UINT8  : for(int k=0; k<elements; ++k) { controller.checkPointNext(); consumeUInt8  (); } break;
			case UINT16 : for(int k=0; k<elements; ++k) { controller.checkPointNext(); consumeUInt16 (); } break;
			case UINT32 : for(int k=0; k<elements; ++k) { controller.checkPointNext(); consumeUInt32 (); } break;
			case UINT64 : for(int k=0; k<elements; ++k) { controller.checkPointNext(); consumeUInt64 (); } break;
			default:
				throw new MLIOException("Cannot skip in a stream of " + _targetType + " elements");
		}
	}
	
	/**
	 * Consume a boolean-valued element
	 */
	boolean consumeLogical() throws IOException
	{
		if(_buffer.remaining()==0) {
			loadNextBuffer();
		}
		return _converter.asLogical(_buffer);
	}
	
	/**
	 * Consume a list of boolean-valued elements
	 */
	void consumeLogical(boolean[] out) throws IOException
	{
		for(int k=0; k<out.length; ++k) {
			out[k] = consumeLogical();
		}
	}
	
	/**
	 * Consume a list of boolean-valued elements
	 */
	void consumeLogical(boolean[] out, Controller controller) throws IOException, CanceledByUser
	{
		for(int k=0; k<out.length; ++k) {
			controller.checkPointNext();
			out[k] = consumeLogical();
		}
	}
	
	/**
	 * Consume a char-valued element
	 */
	char consumeChar() throws IOException
	{
		if(_buffer.remaining()<4 && _remains!=0) {
			loadNextBuffer();
		}
		return _converter.asChar(_buffer);
	}
	
	/**
	 * Consume a list of char-valued elements
	 */
	void consumeChar(char[] out) throws IOException
	{
		for(int k=0; k<out.length; ++k) {
			out[k] = consumeChar();
		}
	}
	
	/**
	 * Consume a list of char-valued elements
	 */
	void consumeChar(char[] out, Controller controller) throws IOException, CanceledByUser
	{
		for(int k=0; k<out.length; ++k) {
			controller.checkPointNext();
			out[k] = consumeChar();
		}
	}
	
	/**
	 * Consume a double-valued element
	 */
	double consumeDouble() throws IOException
	{
		if(_buffer.remaining()==0) {
			loadNextBuffer();
		}
		return _converter.asDouble(_buffer);
	}
	
	/**
	 * Consume a list of double-valued elements
	 */
	void consumeDouble(double[] out) throws IOException
	{
		for(int k=0; k<out.length; ++k) {
			out[k] = consumeDouble();
		}
	}
	
	/**
	 * Consume a list of double-valued elements
	 */
	void consumeDouble(double[] out, Controller controller) throws IOException, CanceledByUser
	{
		for(int k=0; k<out.length; ++k) {
			controller.checkPointNext();
			out[k] = consumeDouble();
		}
	}
	
	/**
	 * Consume a single-valued element
	 */
	float consumeSingle() throws IOException
	{
		if(_buffer.remaining()==0) {
			loadNextBuffer();
		}
		return _converter.asSingle(_buffer);
	}
	
	/**
	 * Consume a list of single-valued elements
	 */
	void consumeSingle(float[] out) throws IOException
	{
		for(int k=0; k<out.length; ++k) {
			out[k] = consumeSingle();
		}
	}
	
	/**
	 * Consume a list of single-valued elements
	 */
	void consumeSingle(float[] out, Controller controller) throws IOException, CanceledByUser
	{
		for(int k=0; k<out.length; ++k) {
			controller.checkPointNext();
			out[k] = consumeSingle();
		}
	}
	
	/**
	 * Consume a int8-valued element
	 */
	byte consumeInt8() throws IOException
	{
		if(_buffer.remaining()==0) {
			loadNextBuffer();
		}
		return _converter.asInt8(_buffer);
	}
	
	/**
	 * Consume a list of int8-valued elements
	 */
	void consumeInt8(byte[] out) throws IOException
	{
		for(int k=0; k<out.length; ++k) {
			out[k] = consumeInt8();
		}
	}
	
	/**
	 * Consume a list of int8-valued elements
	 */
	void consumeInt8(byte[] out, Controller controller) throws IOException, CanceledByUser
	{
		for(int k=0; k<out.length; ++k) {
			controller.checkPointNext();
			out[k] = consumeInt8();
		}
	}
	
	/**
	 * Consume a int16-valued element
	 */
	short consumeInt16() throws IOException
	{
		if(_buffer.remaining()==0) {
			loadNextBuffer();
		}
		return _converter.asInt16(_buffer);
	}
	
	/**
	 * Consume a list of int16-valued elements
	 */
	void consumeInt16(short[] out) throws IOException
	{
		for(int k=0; k<out.length; ++k) {
			out[k] = consumeInt16();
		}
	}
	
	/**
	 * Consume a list of int16-valued elements
	 */
	void consumeInt16(short[] out, Controller controller) throws IOException, CanceledByUser
	{
		for(int k=0; k<out.length; ++k) {
			controller.checkPointNext();
			out[k] = consumeInt16();
		}
	}
	
	/**
	 * Consume a int32-valued element
	 */
	int consumeInt32() throws IOException
	{
		if(_buffer.remaining()==0) {
			loadNextBuffer();
		}
		return _converter.asInt32(_buffer);
	}
	
	/**
	 * Consume a list of int32-valued elements
	 */
	void consumeInt32(int[] out) throws IOException
	{
		for(int k=0; k<out.length; ++k) {
			out[k] = consumeInt32();
		}
	}
	
	/**
	 * Consume a list of int32-valued elements
	 */
	void consumeInt32(int[] out, Controller controller) throws IOException, CanceledByUser
	{
		for(int k=0; k<out.length; ++k) {
			controller.checkPointNext();
			out[k] = consumeInt32();
		}
	}
	
	/**
	 * Consume a int64-valued element
	 */
	long consumeInt64() throws IOException
	{
		if(_buffer.remaining()==0) {
			loadNextBuffer();
		}
		return _converter.asInt64(_buffer);
	}
	
	/**
	 * Consume a list of int64-valued elements
	 */
	void consumeInt64(long[] out) throws IOException
	{
		for(int k=0; k<out.length; ++k) {
			out[k] = consumeInt64();
		}
	}
	
	/**
	 * Consume a list of int64-valued elements
	 */
	void consumeInt64(long[] out, Controller controller) throws IOException, CanceledByUser
	{
		for(int k=0; k<out.length; ++k) {
			controller.checkPointNext();
			out[k] = consumeInt64();
		}
	}
	
	/**
	 * Consume a uint8-valued element
	 */
	byte consumeUInt8() throws IOException
	{
		if(_buffer.remaining()==0) {
			loadNextBuffer();
		}
		return _converter.asUInt8(_buffer);
	}
	
	/**
	 * Consume a list of uint8-valued elements
	 */
	void consumeUInt8(byte[] out) throws IOException
	{
		for(int k=0; k<out.length; ++k) {
			out[k] = consumeUInt8();
		}
	}
	
	/**
	 * Consume a list of uint8-valued elements
	 */
	void consumeUInt8(byte[] out, Controller controller) throws IOException, CanceledByUser
	{
		for(int k=0; k<out.length; ++k) {
			controller.checkPointNext();
			out[k] = consumeUInt8();
		}
	}
	
	/**
	 * Consume a uint16-valued element
	 */
	short consumeUInt16() throws IOException
	{
		if(_buffer.remaining()==0) {
			loadNextBuffer();
		}
		return _converter.asUInt16(_buffer);
	}
	
	/**
	 * Consume a list of uint16-valued elements
	 */
	void consumeUInt16(short[] out) throws IOException
	{
		for(int k=0; k<out.length; ++k) {
			out[k] = consumeUInt16();
		}
	}
	
	/**
	 * Consume a list of uint16-valued elements
	 */
	void consumeUInt16(short[] out, Controller controller) throws IOException, CanceledByUser
	{
		for(int k=0; k<out.length; ++k) {
			controller.checkPointNext();
			out[k] = consumeUInt16();
		}
	}
	
	/**
	 * Consume a uint32-valued element
	 */
	int consumeUInt32() throws IOException
	{
		if(_buffer.remaining()==0) {
			loadNextBuffer();
		}
		return _converter.asUInt32(_buffer);
	}
	
	/**
	 * Consume a list of uint32-valued elements
	 */
	void consumeUInt32(int[] out) throws IOException
	{
		for(int k=0; k<out.length; ++k) {
			out[k] = consumeUInt32();
		}
	}
	
	/**
	 * Consume a list of uint32-valued elements
	 */
	void consumeUInt32(int[] out, Controller controller) throws IOException, CanceledByUser
	{
		for(int k=0; k<out.length; ++k) {
			controller.checkPointNext();
			out[k] = consumeUInt32();
		}
	}
	
	/**
	 * Consume a uint64-valued element
	 */
	long consumeUInt64() throws IOException
	{
		if(_buffer.remaining()==0) {
			loadNextBuffer();
		}
		return _converter.asUInt64(_buffer);
	}
	
	/**
	 * Consume a list of uint64-valued elements
	 */
	void consumeUInt64(long[] out) throws IOException
	{
		for(int k=0; k<out.length; ++k) {
			out[k] = consumeUInt64();
		}
	}
	
	/**
	 * Consume a list of uint64-valued elements
	 */
	void consumeUInt64(long[] out, Controller controller) throws IOException, CanceledByUser
	{
		for(int k=0; k<out.length; ++k) {
			controller.checkPointNext();
			out[k] = consumeUInt64();
		}
	}
	
	/**
	 * Re-fill the buffer
	 */
	private void loadNextBuffer() throws IOException
	{
		int alreadyInBuffer = _buffer==null ? 0 : _buffer.remaining();
		int toBeRead        = Math.min(CHUNK_SIZE - alreadyInBuffer, _remains);
		int newBufferLength = toBeRead + alreadyInBuffer;
		if(_buffer!=null && toBeRead<=0) {
			throw new MLUtil.EndOfStream();
		}
		ByteBuffer newBuffer = MLUtil.allocate(_endianness, newBufferLength);
		if(_buffer!=null && alreadyInBuffer>0) {
			MLUtil.copy(newBuffer, 0, _buffer, _buffer.position(), alreadyInBuffer);
			newBuffer.position(alreadyInBuffer);
		}
		_buffer = newBuffer;
		MLUtil.consume(_stream, _buffer, _buffer.position(), toBeRead);
		_remains -= toBeRead;
		if(_remains==0) {
			MLUtil.consume(_stream, _endianness, _header.getPaddingLength());
		}
	}
	
	/**
	 * Interface for type conversion on the fly
	 */
	private static abstract class Converter
	{
		/**
		 * Conversion to logical
		 */
		public boolean asLogical(ByteBuffer input) throws MLIOException
		{
			throw new MLIOException("Conversion to logical forbidden");
		}
		
		/**
		 * Conversion to char
		 */
		public char asChar(ByteBuffer input) throws MLIOException
		{
			throw new MLIOException("Conversion to char forbidden");
		}
		
		/**
		 * Conversion to double
		 */
		public double asDouble(ByteBuffer input) throws MLIOException
		{
			throw new MLIOException("Conversion to double forbidden");
		}
		
		/**
		 * Conversion to single
		 */
		public float asSingle(ByteBuffer input) throws MLIOException
		{
			throw new MLIOException("Conversion to single forbidden");
		}
		
		/**
		 * Conversion to int8
		 */
		public byte asInt8(ByteBuffer input) throws MLIOException
		{
			throw new MLIOException("Conversion to int8 forbidden");
		}
		
		/**
		 * Conversion to int16
		 */
		public short asInt16(ByteBuffer input) throws MLIOException
		{
			throw new MLIOException("Conversion to int16 forbidden");
		}
		
		/**
		 * Conversion to int32
		 */
		public int asInt32(ByteBuffer input) throws MLIOException
		{
			throw new MLIOException("Conversion to int32 forbidden");
		}
		
		/**
		 * Conversion to int64
		 */
		public long asInt64(ByteBuffer input) throws MLIOException
		{
			throw new MLIOException("Conversion to int64 forbidden");
		}
		
		/**
		 * Conversion to uint8
		 */
		public byte asUInt8(ByteBuffer input) throws MLIOException
		{
			throw new MLIOException("Conversion to uint8 forbidden");
		}
		
		/**
		 * Conversion to uint16
		 */
		public short asUInt16(ByteBuffer input) throws MLIOException
		{
			throw new MLIOException("Conversion to uint16 forbidden");
		}
		
		/**
		 * Conversion to uint32
		 */
		public int asUInt32(ByteBuffer input) throws MLIOException
		{
			throw new MLIOException("Conversion to uint32 forbidden");
		}
		
		/**
		 * Conversion to uint64
		 */
		public long asUInt64(ByteBuffer input) throws MLIOException
		{
			throw new MLIOException("Conversion to uint64 forbidden");
		}
	}
	
	/**
	 * Default converter, performing no conversion (just a bulk copy)
	 */
	private static class CopyConverter extends Converter
	{
		@Override
		public char asChar(ByteBuffer input) throws MLIOException
		{
			return input.getChar();
		}
		
		@Override
		public double asDouble(ByteBuffer input) throws MLIOException
		{
			return input.getDouble();
		}
		
		@Override
		public float asSingle(ByteBuffer input) throws MLIOException
		{
			return input.getFloat();
		}
		
		@Override
		public byte asInt8(ByteBuffer input) throws MLIOException
		{
			return input.get();
		}
		
		@Override
		public short asInt16(ByteBuffer input) throws MLIOException
		{
			return input.getShort();
		}
		
		@Override
		public int asInt32(ByteBuffer input) throws MLIOException
		{
			return input.getInt();
		}
		
		@Override
		public long asInt64(ByteBuffer input) throws MLIOException
		{
			return input.getLong();
		}
		
		@Override
		public byte asUInt8(ByteBuffer input) throws MLIOException
		{
			return input.get();
		}
		
		@Override
		public short asUInt16(ByteBuffer input) throws MLIOException
		{
			return input.getShort();
		}
		
		@Override
		public int asUInt32(ByteBuffer input) throws MLIOException
		{
			return input.getInt();
		}
		
		@Override
		public long asUInt64(ByteBuffer input) throws MLIOException
		{
			return input.getLong();
		}
	}
	
	/**
	 * Boolean extraction
	 */
	private static class LogicalConverter extends Converter
	{
		@Override
		public boolean asLogical(ByteBuffer input) throws MLIOException
		{
			return input.get()!=0;
		}
	}
	
	/**
	 * Charset conversions
	 */
	private static class CharConverter extends Converter
	{
		private CharsetDecoder _decoder;
		
		public CharConverter(String charsetName) throws MLIOException
		{
			try {
				_decoder = Charset.forName(charsetName).newDecoder();
			}
			catch(IllegalCharsetNameException e) {
				throw new MLIOException("Unknown charset: " + charsetName);
			}
		}
		
		@Override
		public char asChar(ByteBuffer input) throws MLIOException
		{
			ByteBuffer output = ByteBuffer.wrap(new byte[2]);
			if(convertChars(output, input)!=1) {
				throw new MLIOException("Unable to extract a char from the current stream");
			}
			return output.getChar();
		}
		
		/**
		 * Core conversion function
		 */
		private int convertChars(ByteBuffer output, ByteBuffer input)
		{
			CharBuffer castedOutput = output.asCharBuffer();
			_decoder.decode(input, castedOutput, true);
			return castedOutput.position();
		}
	}
	
	/**
	 * Single conversions
	 */
	private static abstract class SingleConverter extends Converter
	{
		@Override
		public float asSingle(ByteBuffer input) throws MLIOException
		{
			return convertNextSingle(input);
		}
		
		/**
		 * Core conversion function
		 */
		protected abstract float convertNextSingle(ByteBuffer input);
	}
	
	/**
	 * Double conversions
	 */
	private static abstract class DoubleConverter extends Converter
	{
		@Override
		public double asDouble(ByteBuffer input) throws MLIOException
		{
			return convertNextDouble(input);
		}
		
		/**
		 * Core conversion function
		 */
		protected abstract double convertNextDouble(ByteBuffer input);
	}
	
	/**
	 * Return a type converter object, or throw an exception if the type conversion is not possible
	 */
	private static Converter buildConverter(MLType targetType, MLRawType rawType)
		throws MLIOException
	{
		switch(targetType)
		{
			// Boolean case
			case LOGICAL:
				switch(rawType)
				{
					case UINT8: return new LogicalConverter();
					default: break;
				}
				break;
			
			// Char cases
			case CHAR:
				switch(rawType)
				{	
					case UTF16: return new CopyConverter();
					case UTF8 : return new CharConverter("UTF-8");
					default: break;
				}
				break;
			
			// Integer cases
			case INT8  : if(rawType==MLRawType.INT8  ) return new CopyConverter(); break;
			case UINT8 : if(rawType==MLRawType.UINT8 ) return new CopyConverter(); break;
			case INT16 : if(rawType==MLRawType.INT16 ) return new CopyConverter(); break;
			case UINT16: if(rawType==MLRawType.UINT16) return new CopyConverter(); break;
			case INT32 : if(rawType==MLRawType.INT32 ) return new CopyConverter(); break;
			case UINT32: if(rawType==MLRawType.UINT32) return new CopyConverter(); break;
			case INT64 : if(rawType==MLRawType.INT64 ) return new CopyConverter(); break;
			case UINT64: if(rawType==MLRawType.UINT64) return new CopyConverter(); break;
			
			// Float case
			case SINGLE:
				switch(rawType)
				{
					// Native case
					case SINGLE: return new CopyConverter();
					
					// Int8 to single
					case INT8: return new SingleConverter()
					{
						@Override
						protected float convertNextSingle(ByteBuffer input) {
							return input.get();
						}
					};
					
					// UInt8 to single
					case UINT8: return new SingleConverter()
					{
						@Override
						protected float convertNextSingle(ByteBuffer input) {
							float v = input.get();
							if(v<0) {
								v += 256;
							}
							return v;
						}
					};
					
					// Int16 to single
					case INT16: return new SingleConverter()
					{
						@Override
						protected float convertNextSingle(ByteBuffer input) {
							return input.getShort();
						}
					};
					
					// UInt16 to single
					case UINT16: return new SingleConverter()
					{
						@Override
						protected float convertNextSingle(ByteBuffer input) {
							float v = input.getShort();
							if(v<0) {
								v += 65536;
							}
							return v;
						}
					};
					
					// Int32 to single
					case INT32: return new SingleConverter()
					{
						@Override
						protected float convertNextSingle(ByteBuffer input) {
							return input.getInt();
						}
					};
					
					// UInt32 to single
					case UINT32: return new SingleConverter()
					{
						@Override
						protected float convertNextSingle(ByteBuffer input) {
							float v = input.getInt();
							if(v<0) {
								v += (65536L * 65536L);
							}
							return v;
						}
					};
					
					// Other cases
					default:
						break;
				}
				break;
			
			// Double case
			case DOUBLE:
				switch(rawType)
				{	
					// Native case
					case DOUBLE: return new CopyConverter();
					
					// Single to double
					case SINGLE: return new DoubleConverter()
					{
						@Override
						protected double convertNextDouble(ByteBuffer input) {
							return input.getFloat();
						}
					};
					
					// Int8 to double
					case INT8: return new DoubleConverter()
					{
						@Override
						protected double convertNextDouble(ByteBuffer input) {
							return input.get();
						}
					};
					
					// UInt8 to double
					case UINT8: return new DoubleConverter()
					{
						@Override
						protected double convertNextDouble(ByteBuffer input) {
							double v = input.get();
							if(v<0) {
								v += 256;
							}
							return v;
						}
					};
					
					// Int16 to double
					case INT16: return new DoubleConverter()
					{
						@Override
						protected double convertNextDouble(ByteBuffer input) {
							return input.getShort();
						}
					};
					
					// UInt16 to double
					case UINT16: return new DoubleConverter()
					{
						@Override
						protected double convertNextDouble(ByteBuffer input) {
							double v = input.getShort();
							if(v<0) {
								v += 65536;
							}
							return v;
						}
					};
					
					// Int32 to double
					case INT32: return new DoubleConverter()
					{
						@Override
						protected double convertNextDouble(ByteBuffer input) {
							return input.getInt();
						}
					};
					
					// UInt32 to double
					case UINT32: return new DoubleConverter()
					{
						@Override
						protected double convertNextDouble(ByteBuffer input) {
							double v = input.getInt();
							if(v<0) {
								v += (65536L * 65536L);
							}
							return v;
						}
					};
					
					// Int64 to double
					case INT64: return new DoubleConverter()
					{
						@Override
						protected double convertNextDouble(ByteBuffer input) {
							return input.getLong();
						}
					};
					
					// UInt64 to double
					case UINT64: return new DoubleConverter()
					{
						@Override
						protected double convertNextDouble(ByteBuffer input) {
							double v = input.getLong();
							if(v<0) {
								v += Long.MAX_VALUE;
								v += Long.MAX_VALUE;
								v += 2;
							}
							return v;
						}
					};
					
					// Other cases
					default:
						break;
				}
				break;
			
			// Other cases
			default:
				break;
		}
		throw new MLIOException("Cannot interpret " + rawType + " as " + targetType);
	}
}
