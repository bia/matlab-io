package plugins.ylemontag.matlabio.lib;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.zip.InflaterInputStream;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Matlab .mat file reader
 */
public class MatFileReader
{
	/**
	 * The Matlab variable whose size is less or equal to this parameter will be
	 * buffered while reading the index of the Matlab file
	 */
	private static final int SMALL_ELEMENT_SIZE = 0x10000;
	
	private File _file;
	private ByteOrder _endianness;
	private Map<String, Element> _index;
	
	/**
	 * Element of a .mat file (not decoded)
	 */
	private static class RawElement
	{
		public MLRawType type;
		public StreamBuilder streamBuilder;
		public RawElement(MLRawType srcType, StreamBuilder srcStreamBuilder)
		{
			type = srcType;
			streamBuilder = srcStreamBuilder;
		}
	}
	
	/**
	 * Element of a .mat file (decoded)
	 */
	private static class Element
	{
		public MLMeta meta;
		public StreamBuilder streamBuilder;
		public Element(MLMeta srcMeta, StreamBuilder srcStreamBuilder)
		{
			meta = srcMeta;
			streamBuilder = srcStreamBuilder;
		}
	}
	
	/**
	 * Open a .mat file for reading
	 */
	public MatFileReader(String path) throws IOException
	{
		this(new File(path));
	}
	
	/**
	 * Open a .mat file for reading
	 */
	public MatFileReader(File file) throws IOException
	{	
		// Initialize objects
		if(file==null) {
			throw new MLIOException("No file selected");
		}
		_file = file;
		_endianness = ByteOrder.BIG_ENDIAN; // Will be changed when reading the file header
		_index = new HashMap<String, Element>();
		
		// Analyzing the file 
		FileInputStream mainStream = new FileInputStream(_file);
		readHeader(mainStream);
		LinkedList<RawElement> rawElems = readRawElements(mainStream);
		mainStream.close();
		LinkedList<Element> elems = decodeElements(rawElems);
		
		// Feeding the index
		for(Element elem : elems) {
			_index.put(elem.meta.getName(), elem);
		}
	}
	
	/**
	 * Return the underlying file
	 */
	public File getFile()
	{
		return _file;
	}
	
	/**
	 * Return the endianness used for this file
	 */
	public ByteOrder getEndianness()
	{
		return _endianness;
	}
	
	/**
	 * List of the variables contained in the Matlab .mat file
	 */
	public Set<String> getKeys()
	{
		return _index.keySet();
	}
	
	/**
	 * Return the meta-data associated to a given variable
	 * @return null if the key does not exist
	 */
	public MLMeta getMeta(String key)
	{
		Element elem = _index.get(key);
		if(elem==null) {
			return null;
		}
		return elem.meta;
	}
	
	/**
	 * Return the variable named 'key'
	 * @throw MLIOException If the given key does not exist
	 */
	public MLArray getData(String key) throws IOException
	{
		try {
			return getData(key, new Controller());
		}
		catch(Controller.CanceledByUser err) {
			throw new RuntimeException("A Matlab array import operation have been unexpectedly interrupted");
		}
	}
	
	/**
	 * Return the variable named 'key'
	 * @throw MLIOException If the given key does not exist
	 */
	public MLArray getData(String key, Controller controller)
		throws IOException, Controller.CanceledByUser
	{
		Element elem = _index.get(key);
		if(elem==null) {
			throw new MLIOException("Cannot find variable named " + key);
		}
		return retrieveArray(elem, controller);
	}
	
	/**
	 * Return the variable named 'key' as a stream
	 * @throw MLIOException If the given key does not exist
	 */
	public MLIStream getDataAsStream(String key) throws IOException
	{
		Element elem = _index.get(key);
		if(elem==null) {
			throw new MLIOException("Cannot find variable named " + key);
		}
		return retrieveStream(elem);
	}
	
	/**
	 * Read the header
	 */
	private void readHeader(FileInputStream stream) throws IOException
	{
		ByteBuffer buffer = MLUtil.consume(stream, _endianness, 128);
		
		// Check the description field
		if(!(buffer.get()=='M' && buffer.get()=='A' && buffer.get()=='T' && buffer.get()=='L')) {
			throw new MLIOException("Not a MATLAB 5.0 MAT-file");
		}
		
		// Endianness indicator
		if(buffer.get(126)=='I' && buffer.get(127)=='M') {
			_endianness = ByteOrder.LITTLE_ENDIAN;
		}
		else if(buffer.get(126)=='M' && buffer.get(127)=='I') {
			_endianness = ByteOrder.BIG_ENDIAN;
		}
		else {
			throw new MLIOException("Invalid endianness indicator");
		}
		
		// Check the version flag
		byte versionHi = buffer.get(_endianness==ByteOrder.BIG_ENDIAN ? 124 : 125);
		byte versionLo = buffer.get(_endianness==ByteOrder.BIG_ENDIAN ? 125 : 124);
		if(!(versionHi==0x01 && versionLo==0x00)) {
			throw new MLIOException("Invalid version flag");
		}
	}
	
	/**
	 * Read the list of elements contained in the file
	 */
	private LinkedList<RawElement> readRawElements(FileInputStream stream) throws IOException
	{
		LinkedList<RawElement> retVal = new LinkedList<RawElement>();
		try {
			while(true) {
				retVal.add(readRawElement(stream));
			}
		}
		catch(MLUtil.EndOfStream err) {}
		return retVal;
	}
	
	/**
	 * Read an element, i.e. decode its tag
	 */
	private RawElement readRawElement(FileInputStream stream) throws IOException
	{
		ByteBuffer buffer = MLUtil.consume(stream, _endianness, 8);
		MLRawType type = MLRawType.byteToMLRawType((byte)buffer.getInt());
		int length = buffer.getInt();
		if(length<0) {
			throw new MLIOException("Negative segment length detected");
		}
		StreamBuilder streamBuilder;
		if(length>=SMALL_ELEMENT_SIZE) {
			long currentPosition = stream.getChannel().position();
			streamBuilder = new FileInputStreamBuilder(_file, currentPosition);
			stream.skip(length);
		}
		else {
			ByteBuffer dataBuffer = MLUtil.consume(stream, _endianness, length);
			streamBuilder = new ByteBufferStreamBuilder(dataBuffer);
		}
		return new RawElement(type, streamBuilder);
	}
	
	/**
	 * Decode all elements
	 */
	private LinkedList<Element> decodeElements(LinkedList<RawElement> rawElems) throws IOException
	{
		LinkedList<Element> retVal = new LinkedList<Element>();
		for(RawElement rawElem : rawElems) {
			Element elem = decodeElement(rawElem);
			retVal.add(elem);
		}
		return retVal;
	}
	
	/**
	 * Decode an element from a raw element
	 */
	private Element decodeElement(RawElement rawElem) throws IOException
	{
		switch(rawElem.type) {
			case MATRIX:
				return decodeMatrixElement(rawElem.streamBuilder);
			case COMPRESSED:
				return decodeCompressedElement(rawElem.streamBuilder);
			default:
				throw new MLIOException("Unexpected raw element type: " + rawElem.type);
		}
	}
	
	/**
	 * Decode a compressed element
	 */
	private Element decodeCompressedElement(StreamBuilder streamBuilder) throws IOException
	{
		StreamBuilder inflaterStreamBuilder  = new InflaterStreamBuilder(streamBuilder);
		StreamBuilder nextLevelStreamBuilder = new ShifterStreamBuilder(inflaterStreamBuilder, 8);
		ByteBuffer compressedHeader = MLUtil.consume(inflaterStreamBuilder.build(), _endianness, 8);
		MLRawType compressedType = MLRawType.byteToMLRawType((byte)compressedHeader.getInt());
		if(compressedType!=MLRawType.MATRIX) {
			throw new MLIOException("Unexpected compressed raw element type: " + compressedType);
		}
		return decodeMatrixElement(nextLevelStreamBuilder);
	}
	
	/**
	 * Decode a matrix element
	 */
	private Element decodeMatrixElement(StreamBuilder streamBuilder) throws IOException
	{
		MLUtil.Field field;
		InputStream stream = streamBuilder.build();
		int offset = 0;
		
		// Array flag sub-element
		field = MLUtil.readField(stream, _endianness);
		offset += field.getTotalLength();
		if(field.getType()!=MLRawType.UINT32) {
			throw new MLIOException("Badly formatted array flag subelement");
		}
		int rawFlags = field.getData().getInt(0);
		MLType mlClass = MLType.byteToMLType((byte)(rawFlags & 0x000000ff));
		boolean isComplex = (rawFlags & 0x00000800)!=0;
		boolean isLogical = (rawFlags & 0x00000200)!=0;
		if(isLogical) {
			if(mlClass!=MLType.UINT8) {
				throw new MLIOException("Badly encoded logical array");
			}
			mlClass = MLType.LOGICAL;
		}
		
		// Dimensions sub-element
		field = MLUtil.readField(stream, _endianness);
		offset += field.getTotalLength();
		if(field.getType()!=MLRawType.INT32) {
			throw new MLIOException("Badly formatted dimensions subelement");
		}
		IntBuffer rawDims = field.getData().asIntBuffer();
		int nDims = rawDims.limit();
		int[] dims = new int[nDims];
		for(int k=0; k<nDims; ++k) {
			dims[k] = rawDims.get(k);
		}
		
		// Name sub-element
		field = MLUtil.readField(stream, _endianness);
		offset += field.getTotalLength();
		if(field.getType()!=MLRawType.INT8) {
			throw new MLIOException("Badly formatted name subelement");
		}
		ByteBuffer rawName = field.getData();
		int szName = rawName.limit();
		String name = "";
		for(int k=0; k<szName; ++k) {
			name += (char)rawName.get(k);
		}
		
		// Returned value
		MLMeta meta = new MLMeta(mlClass, name, dims, isComplex);
		Element retVal = new Element(meta, new ShifterStreamBuilder(streamBuilder, offset));
		return retVal;
	}
	
	/**
	 * Read the content associated to an object in a Matlab .mat file 
	 */
	private MLArray retrieveArray(Element elem, Controller controller)
		throws IOException, Controller.CanceledByUser
	{
		controller.checkPoint();
		MLType type = elem.meta.getType();
		MLArray retVal = null;
		switch(type) {
			case INT8  :
			case UINT8 :
			case INT16 :
			case UINT16:
			case INT32 :
			case UINT32:
			case INT64 :
			case UINT64:
			case SINGLE:
			case DOUBLE:
				retVal = retrieveNumericArray(elem, controller);
				break;
			case CHAR:
				retVal = retrieveCharArray(elem, controller);
				break;
			case LOGICAL:
				retVal = retrieveLogicalArray(elem, controller);
				break;
			default:
				throw new MLIOException("Import of " + type + " objects not implemented yet");
		}
		return retVal;
	}
	
	/**
	 * Read the content associated to a numeric array 
	 */
	private MLArrays.Numeric<?> retrieveNumericArray(Element elem, Controller controller)
		throws IOException, Controller.CanceledByUser
	{
		InputStream stream    = elem.streamBuilder.build();
		MLType      type      = elem.meta.getType();
		boolean     isComplex = elem.meta.getIsComplex();
		int         elements  = elem.meta.getSize();
		if(isComplex) {
			elements *= 2;
		}
		
		// Allocate the result
		MLArrays.Numeric<?> retVal;
		switch(type) {
			case INT8  : retVal = new MLArrays.Int8  (elem.meta); break;
			case UINT8 : retVal = new MLArrays.UInt8 (elem.meta); break;
			case INT16 : retVal = new MLArrays.Int16 (elem.meta); break;
			case UINT16: retVal = new MLArrays.UInt16(elem.meta); break;
			case INT32 : retVal = new MLArrays.Int32 (elem.meta); break;
			case UINT32: retVal = new MLArrays.UInt32(elem.meta); break;
			case INT64 : retVal = new MLArrays.Int64 (elem.meta); break;
			case UINT64: retVal = new MLArrays.UInt64(elem.meta); break;
			case SINGLE: retVal = new MLArrays.Single(elem.meta); break;
			case DOUBLE: retVal = new MLArrays.Double(elem.meta); break;
			default:
				throw new MLIOException("Cannot generate a numeric array from a " + type + " object");
		}
		
		// Feed the array
		controller.startCounter(elements);
		MLDataInputStream realPart = new MLDataInputStream(stream, _endianness, type);
		feedNumericArray(realPart, retVal, true, controller);
		if(isComplex) {
			MLDataInputStream imaginaryPart = new MLDataInputStream(stream, _endianness, type);
			feedNumericArray(imaginaryPart, retVal, false, controller);
		}
		controller.stopCounter();
		return retVal;
	}
	
	/**
	 * Feed a section (either the real part or the imaginary part) of a MLNumericArray
	 */
	private void feedNumericArray(MLDataInputStream dataStream, MLArrays.Numeric<?> target, boolean isRealPart,
		Controller controller) throws IOException, Controller.CanceledByUser
	{
		if(isRealPart) {
			switch(target.getType()) {
				case INT8  : dataStream.consumeInt8  (((MLArrays.Int8  )target).getReal(), controller); break;
				case UINT8 : dataStream.consumeUInt8 (((MLArrays.UInt8 )target).getReal(), controller); break;
				case INT16 : dataStream.consumeInt16 (((MLArrays.Int16 )target).getReal(), controller); break;
				case UINT16: dataStream.consumeUInt16(((MLArrays.UInt16)target).getReal(), controller); break;
				case INT32 : dataStream.consumeInt32 (((MLArrays.Int32 )target).getReal(), controller); break;
				case UINT32: dataStream.consumeUInt32(((MLArrays.UInt32)target).getReal(), controller); break;
				case INT64 : dataStream.consumeInt64 (((MLArrays.Int64 )target).getReal(), controller); break;
				case UINT64: dataStream.consumeUInt64(((MLArrays.UInt64)target).getReal(), controller); break;
				case SINGLE: dataStream.consumeSingle(((MLArrays.Single)target).getReal(), controller); break;
				case DOUBLE: dataStream.consumeDouble(((MLArrays.Double)target).getReal(), controller); break;
				default:
					throw new MLIOException("Cannot generate a numeric array from a " + target.getType() + " object");
			}
		}
		else {
			switch(target.getType()) {
				case INT8  : dataStream.consumeInt8  (((MLArrays.Int8  )target).getImaginary(), controller); break;
				case UINT8 : dataStream.consumeUInt8 (((MLArrays.UInt8 )target).getImaginary(), controller); break;
				case INT16 : dataStream.consumeInt16 (((MLArrays.Int16 )target).getImaginary(), controller); break;
				case UINT16: dataStream.consumeUInt16(((MLArrays.UInt16)target).getImaginary(), controller); break;
				case INT32 : dataStream.consumeInt32 (((MLArrays.Int32 )target).getImaginary(), controller); break;
				case UINT32: dataStream.consumeUInt32(((MLArrays.UInt32)target).getImaginary(), controller); break;
				case INT64 : dataStream.consumeInt64 (((MLArrays.Int64 )target).getImaginary(), controller); break;
				case UINT64: dataStream.consumeUInt64(((MLArrays.UInt64)target).getImaginary(), controller); break;
				case SINGLE: dataStream.consumeSingle(((MLArrays.Single)target).getImaginary(), controller); break;
				case DOUBLE: dataStream.consumeDouble(((MLArrays.Double)target).getImaginary(), controller); break;
				default:
					throw new MLIOException("Cannot generate a numeric array from a " + target.getType() + " object");
			}
		}
	}
	
	/**
	 * Read the content associated to a char array
	 */
	private MLArrays.Char retrieveCharArray(Element elem, Controller controller)
		throws IOException, Controller.CanceledByUser
	{
		InputStream stream = elem.streamBuilder.build();
		MLDataInputStream dataStream = new MLDataInputStream(stream, _endianness, MLType.CHAR);
		MLArrays.Char retVal = new MLArrays.Char(elem.meta);
		controller.startCounter(elem.meta.getSize());
		dataStream.consumeChar(retVal.get(), controller);
		controller.stopCounter();
		return retVal;
	}
	
	/**
	 * Read the content associated to a char array
	 */
	private MLArrays.Logical retrieveLogicalArray(Element elem, Controller controller)
		throws IOException, Controller.CanceledByUser
	{
		InputStream stream = elem.streamBuilder.build();
		MLDataInputStream dataStream = new MLDataInputStream(stream, _endianness, MLType.LOGICAL);
		MLArrays.Logical retVal = new MLArrays.Logical(elem.meta);
		controller.startCounter(elem.meta.getSize());
		dataStream.consumeLogical(retVal.get(), controller);
		controller.stopCounter();
		return retVal;
	}
	
	/**
	 * Prepare the stream to read an object in a Matlab .mat file 
	 */
	private MLIStream retrieveStream(Element elem) throws IOException
	{
		MLType type = elem.meta.getType();
		MLIStream retVal = null;
		switch(type) {
			case LOGICAL: /// TODO: MLIStreams.Logical should not inherit from MLIStreams.Numeric
			case INT8  :
			case UINT8 :
			case INT16 :
			case UINT16:
			case INT32 :
			case UINT32:
			case INT64 :
			case UINT64:
			case SINGLE:
			case DOUBLE:
				retVal = retrieveNumericStream(elem);
				break;
			default:
				throw new MLIOException("Import of " + type + " objects not implemented yet");
		}
		return retVal;
	}
	
	/**
	 * Prepare the stream associated to a numeric array 
	 */
	private MLIStreams.Numeric retrieveNumericStream(Element elem) throws IOException
	{
		MLType type = elem.meta.getType();
		InputStream rawStream = elem.streamBuilder.build();
		switch(type) {
			case LOGICAL: return new MLIStreams.Logical(elem.meta, rawStream, _endianness); //TODO: remove from retrieveNumericStream
			case INT8  : return new MLIStreams.Int8  (elem.meta, rawStream, _endianness);
			case UINT8 : return new MLIStreams.UInt8 (elem.meta, rawStream, _endianness);
			case INT16 : return new MLIStreams.Int16 (elem.meta, rawStream, _endianness);
			case UINT16: return new MLIStreams.UInt16(elem.meta, rawStream, _endianness);
			case INT32 : return new MLIStreams.Int32 (elem.meta, rawStream, _endianness);
			case UINT32: return new MLIStreams.UInt32(elem.meta, rawStream, _endianness);
			case INT64 : return new MLIStreams.Int64 (elem.meta, rawStream, _endianness);
			case UINT64: return new MLIStreams.UInt64(elem.meta, rawStream, _endianness);
			case SINGLE: return new MLIStreams.Single(elem.meta, rawStream, _endianness);
			case DOUBLE: return new MLIStreams.Double(elem.meta, rawStream, _endianness);
			default:
					throw new MLIOException("Import of " + type + " objects not implemented yet");
		}
	}
	
	/**
	 * Stream builder interface, that is used to build one stream for each raw element
	 */
	private static interface StreamBuilder
	{
		/**
		 * Open a new stream
		 */
		public InputStream build() throws IOException;
	}
	
	/**
	 * Stream builder for a ByteBuffer-based stream
	 */
	private static class ByteBufferStreamBuilder implements StreamBuilder
	{
		private ByteBuffer _data;
		
		/**
		 * Constructor
		 */
		public ByteBufferStreamBuilder(ByteBuffer data)
		{
			_data = data;
		}
		
		@Override
		public InputStream build() throws IOException
		{
			return new ByteArrayInputStream(_data.array());
		}
	}
	
	/**
	 * Stream builder for a FileInputStream-based stream
	 */
	private static class FileInputStreamBuilder implements StreamBuilder
	{
		private File _file;
		private long _offset;
		
		/**
		 * Constructor
		 */
		public FileInputStreamBuilder(File file, long offset)
		{
			_file = file;
			_offset = offset;
		}
		
		@Override
		public InputStream build() throws IOException
		{
			try {
				FileInputStream retVal = new FileInputStream(_file);
				retVal.skip(_offset);
				return retVal;
			}
			catch(FileNotFoundException err) {
				throw new MLIOException("Unable to open a new channel on the current file");
			}
		}
	}
	
	/**
	 * Inflater stream wrapper
	 */
	private static class InflaterStreamBuilder implements StreamBuilder
	{
		private StreamBuilder _in;
		
		/**
		 * Constructor
		 */
		public InflaterStreamBuilder(StreamBuilder in)
		{
			_in = in;
		}

		@Override
		public InputStream build() throws IOException
		{
			return new InflaterInputStream(_in.build());
		}
	}
	
	/**
	 * Shifter stream wrapper
	 */
	private static class ShifterStreamBuilder implements StreamBuilder
	{
		private StreamBuilder _in;
		private int _offset;
		
		/**
		 * Constructor
		 */
		public ShifterStreamBuilder(StreamBuilder in, int offset)
		{
			_in = in;
			_offset = offset;
		}

		@Override
		public InputStream build() throws IOException
		{
			InputStream retVal = _in.build();
			retVal.skip(_offset);
			return retVal;
		}
	}
}
