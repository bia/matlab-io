package plugins.ylemontag.matlabio.lib;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteOrder;

/**
 * 
 * @author Yoann Le Montagner
 *
 * Specialized classes implementing different types of MLIStream
 */
public class MLIStreams
{
	/**
	 * Matlab input stream with boolean values
	 */
	public static class Logical extends Numeric ///TODO: should not inherit from Numeric
	{
		/**
		 * Read the next value
		 */
		public boolean get() throws IOException
		{
			readNextValue();
			return _currentValue;
		}
		
		private boolean _currentValue;
		
		Logical(MLMeta meta, InputStream source, ByteOrder endianness) throws IOException
		{
			super(meta, source, endianness);
			_currentValue = false;
		}
		
		@Override
		protected void loadNextValue(MLDataInputStream stream) throws IOException
		{
			_currentValue = stream.consumeLogical();
		}
	}
	
	/**
	 * Matlab input stream with double values
	 */
	public static class Double extends Numeric
	{
		/**
		 * Read the next value
		 */
		public double get() throws IOException
		{
			readNextValue();
			return _currentValue;
		}
		
		private double _currentValue;
		
		Double(MLMeta meta, InputStream source, ByteOrder endianness) throws IOException
		{
			super(meta, source, endianness);
			_currentValue = 0.0;
		}
		
		@Override
		protected void loadNextValue(MLDataInputStream stream) throws IOException
		{
			_currentValue = stream.consumeDouble();
		}
	}
	
	/**
	 * Matlab input stream with float values
	 */
	public static class Single extends Numeric
	{
		/**
		 * Read the next value
		 */
		public float get() throws IOException
		{
			readNextValue();
			return _currentValue;
		}
		
		private float _currentValue;
		
		Single(MLMeta meta, InputStream source, ByteOrder endianness) throws IOException
		{
			super(meta, source, endianness);
			_currentValue = 0.0f;
		}
		
		@Override
		protected void loadNextValue(MLDataInputStream stream) throws IOException
		{
			_currentValue = stream.consumeSingle();
		}
	}
	
	/**
	 * Matlab input stream with signed 8 bits values
	 */
	public static class Int8 extends Numeric
	{
		/**
		 * Read the next value
		 */
		public byte get() throws IOException
		{
			readNextValue();
			return _currentValue;
		}
		
		private byte _currentValue;
		
		Int8(MLMeta meta, InputStream source, ByteOrder endianness) throws IOException
		{
			super(meta, source, endianness);
			_currentValue = 0;
		}
		
		@Override
		protected void loadNextValue(MLDataInputStream stream) throws IOException
		{
			_currentValue = stream.consumeInt8();
		}
	}
	
	/**
	 * Matlab input stream with signed 16 bits values
	 */
	public static class Int16 extends Numeric
	{
		/**
		 * Read the next value
		 */
		public short get() throws IOException
		{
			readNextValue();
			return _currentValue;
		}
		
		private short _currentValue;
		
		Int16(MLMeta meta, InputStream source, ByteOrder endianness) throws IOException
		{
			super(meta, source, endianness);
			_currentValue = 0;
		}
		
		@Override
		protected void loadNextValue(MLDataInputStream stream) throws IOException
		{
			_currentValue = stream.consumeInt16();
		}
	}
	
	/**
	 * Matlab input stream with signed 32 bits values
	 */
	public static class Int32 extends Numeric
	{
		/**
		 * Read the next value
		 */
		public int get() throws IOException
		{
			readNextValue();
			return _currentValue;
		}
		
		private int _currentValue;
		
		Int32(MLMeta meta, InputStream source, ByteOrder endianness) throws IOException
		{
			super(meta, source, endianness);
			_currentValue = 0;
		}
		
		@Override
		protected void loadNextValue(MLDataInputStream stream) throws IOException
		{
			_currentValue = stream.consumeInt32();
		}
	}
	
	/**
	 * Matlab input stream with signed 64 bits values
	 */
	public static class Int64 extends Numeric
	{
		/**
		 * Read the next value
		 */
		public long get() throws IOException
		{
			readNextValue();
			return _currentValue;
		}
		
		private long _currentValue;
		
		Int64(MLMeta meta, InputStream source, ByteOrder endianness) throws IOException
		{
			super(meta, source, endianness);
			_currentValue = 0;
		}
		
		@Override
		protected void loadNextValue(MLDataInputStream stream) throws IOException
		{
			_currentValue = stream.consumeInt64();
		}
	}
	
	/**
	 * Matlab input stream with unsigned 8 bits values
	 */
	public static class UInt8 extends Numeric
	{
		/**
		 * Read the next value
		 */
		public byte get() throws IOException
		{
			readNextValue();
			return _currentValue;
		}
		
		private byte _currentValue;
		
		UInt8(MLMeta meta, InputStream source, ByteOrder endianness) throws IOException
		{
			super(meta, source, endianness);
			_currentValue = 0;
		}
		
		@Override
		protected void loadNextValue(MLDataInputStream stream) throws IOException
		{
			_currentValue = stream.consumeUInt8();
		}
	}
	
	/**
	 * Matlab input stream with unsigned 16 bits values
	 */
	public static class UInt16 extends Numeric
	{
		/**
		 * Read the next value
		 */
		public short get() throws IOException
		{
			readNextValue();
			return _currentValue;
		}
		
		private short _currentValue;
		
		UInt16(MLMeta meta, InputStream source, ByteOrder endianness) throws IOException
		{
			super(meta, source, endianness);
			_currentValue = 0;
		}
		
		@Override
		protected void loadNextValue(MLDataInputStream stream) throws IOException
		{
			_currentValue = stream.consumeUInt16();
		}
	}
	
	/**
	 * Matlab input stream with unsigned 32 bits values
	 */
	public static class UInt32 extends Numeric
	{
		/**
		 * Read the next value
		 */
		public int get() throws IOException
		{
			readNextValue();
			return _currentValue;
		}
		
		private int _currentValue;
		
		UInt32(MLMeta meta, InputStream source, ByteOrder endianness) throws IOException
		{
			super(meta, source, endianness);
			_currentValue = 0;
		}
		
		@Override
		protected void loadNextValue(MLDataInputStream stream) throws IOException
		{
			_currentValue = stream.consumeUInt32();
		}
	}
	
	/**
	 * Matlab input stream with unsigned 64 bits values
	 */
	public static class UInt64 extends Numeric
	{
		/**
		 * Read the next value
		 */
		public long get() throws IOException
		{
			readNextValue();
			return _currentValue;
		}
		
		private long _currentValue;
		
		UInt64(MLMeta meta, InputStream source, ByteOrder endianness) throws IOException
		{
			super(meta, source, endianness);
			_currentValue = 0;
		}
		
		@Override
		protected void loadNextValue(MLDataInputStream stream) throws IOException
		{
			_currentValue = stream.consumeUInt64();
		}
	}
	
	/**
	 * Matlab numeric input stream
	 */
	static abstract class Numeric extends MLIStream
	{
		/**
		 * Current section in the stream
		 */
		private static enum Section
		{
			REAL     ,
			IMAGINARY,
			EOS      ;
		}
		
		private InputStream       _source        ;
		private ByteOrder         _endianness    ;
		private Section           _currentSection;
		private MLDataInputStream _dataStream    ;
		private int               _nextIndex     ;
		
		/**
		 * Constructor
		 */
		protected Numeric(MLMeta meta, InputStream source, ByteOrder endianness) throws IOException
		{
			super(meta);
			_source = source;
			_endianness = endianness;
			loadNextSection();
		}
		
		/**
		 * Check whether the end of both the real and the imaginary streams has been reached
		 */
		public boolean isAtEnd()
		{
			return _currentSection==Section.EOS;
		}
		
		/**
		 * Return true if the the cursor is not at the end of the stream and if the next
		 * value to be read belongs to the real data stream
		 */
		public boolean isRealPart()
		{
			return _currentSection==Section.REAL;
		}
		
		/**
		 * Return true if the the cursor is not at the end of the stream and if the next
		 * value to be read belongs to the real data stream
		 */
		public boolean isImaginaryPart()
		{
			return _currentSection==Section.IMAGINARY;
		}
		
		/**
		 * Return the index of the next element
		 */
		public int getNextIndex()
		{
			return _nextIndex;
		}
		
		/**
		 * The derived class should save the data element encoded in buffer at buffer.position()
		 * when this function is called
		 */
		protected abstract void loadNextValue(MLDataInputStream stream) throws IOException;
		
		/**
		 * The derived class should call this method at each read request. This method
		 * will call 'loadNextValue' with the correct parameter values
		 */
		protected void readNextValue() throws IOException
		{
			if(_dataStream==null) {
				throw new MLUtil.EndOfStream();
			}
			loadNextValue(_dataStream);
			++_nextIndex;
			if(_dataStream.isAtEnd()) {
				loadNextSection();
			}
		}
		
		/**
		 * Create the data stream corresponding to the next data section (either the
		 * real part or the imaginary part)
		 */
		private void loadNextSection() throws IOException
		{
			// Update the section flag
			if(_currentSection==null) {
				_currentSection = Section.REAL;
			}
			else if(_currentSection==Section.REAL && getIsComplex()) {
				_currentSection = Section.IMAGINARY;
			}
			else {
				_currentSection = Section.EOS;
				_dataStream = null;
				return;
			}
			
			// Load the next section
			_dataStream = new MLDataInputStream(_source, _endianness, getType());
			_nextIndex = 0;
			if(_dataStream.isAtEnd()) {
				_currentSection = Section.EOS;
				_dataStream = null;
			}
		}
		
		@Override
		public void skip(int num) throws IOException
		{
			if(_dataStream==null) {
				throw new MLUtil.EndOfStream();
			}
			int remainingElementsInCurrentSection = getSize()-_nextIndex;
			if(num < remainingElementsInCurrentSection) {
				_nextIndex += num;
				_dataStream.skip(num);
			}
			else {
				_dataStream.skip(remainingElementsInCurrentSection);
				loadNextSection();
				_nextIndex = num - remainingElementsInCurrentSection;
				_dataStream.skip(_nextIndex);
			}
		}
		
		@Override
		public void skip(int num, Controller controller) throws IOException, Controller.CanceledByUser
		{
			if(_dataStream==null) {
				throw new MLUtil.EndOfStream();
			}
			int remainingElementsInCurrentSection = getSize()-_nextIndex;
			if(num < remainingElementsInCurrentSection) {
				_nextIndex += num;
				_dataStream.skip(num, controller);
			}
			else {
				_dataStream.skip(remainingElementsInCurrentSection, controller);
				loadNextSection();
				_nextIndex = num - remainingElementsInCurrentSection;
				_dataStream.skip(_nextIndex, controller);
			}
		}
	}
}
