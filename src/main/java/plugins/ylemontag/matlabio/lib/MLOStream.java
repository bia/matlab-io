package plugins.ylemontag.matlabio.lib;

import java.io.IOException;

import plugins.ylemontag.matlabio.lib.MLMeta;
import plugins.ylemontag.matlabio.lib.MLObject;

/**
 * 
 * @author Yoann Le Montagner
 *
 * Base class used to represent a Matlab data element, whose content is written
 * dynamically to an underlying Matlab file
 */
public abstract class MLOStream extends MLObject
{
	/**
	 * Constructor
	 */
	protected MLOStream(MLMeta meta)
	{
		super(meta);
	}
	
	/**
	 * Skip a given number of elements
	 */
	public abstract void skip(int num) throws IOException;
	
	/**
	 * Skip a given number of elements (with thread-control)
	 */
	public abstract void skip(int num, Controller controller) throws IOException, Controller.CanceledByUser;
}
