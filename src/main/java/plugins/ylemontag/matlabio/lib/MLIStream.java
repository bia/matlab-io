package plugins.ylemontag.matlabio.lib;

import java.io.IOException;

import plugins.ylemontag.matlabio.lib.MLMeta;
import plugins.ylemontag.matlabio.lib.MLObject;
import plugins.ylemontag.matlabio.lib.Controller;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Base class used to represent a Matlab data element, whose content is read
 * dynamically from the underlying Matlab file
 */
public abstract class MLIStream extends MLObject
{	
	/**
	 * Constructor
	 */
	protected MLIStream(MLMeta meta)
	{
		super(meta);
	}
	
	/**
	 * Skip a given number of elements
	 */
	public abstract void skip(int num) throws IOException;
	
	/**
	 * Skip a given number of elements (with thread-control)
	 */
	public abstract void skip(int num, Controller controller) throws IOException, Controller.CanceledByUser;
}
