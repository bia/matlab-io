package plugins.ylemontag.matlabio.lib;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import plugins.ylemontag.matlabio.lib.Controller.CanceledByUser;

/**
 * 
 * @author Yoann Le Montagner
 *
 * Wrap an output stream to write a (potentially) long Matlab field
 */
public class MLDataOutputStream
{
	/**
	 * Maximum size of the buffer
	 */
	private static final int CHUNK_SIZE = 0x1000;
	
	private OutputStream _stream;
	private ByteOrder _endianness;
	private MLType _sourceType;
	private int _encodingTypeSize;
	private MLUtil.FieldHeader _header;
	private int _remains;
	private ByteBuffer _buffer;
	
	/**
	 * Constructor
	 */
	MLDataOutputStream(OutputStream stream, ByteOrder endianness, MLType sourceType, int elements)
		throws IOException
	{
		_stream = stream;
		_endianness = endianness;
		_sourceType = sourceType;
		_encodingTypeSize = sourceType.getRawType().getSize();
		_remains = elements * _encodingTypeSize;
		_header = new MLUtil.FieldHeader(sourceType.getRawType(), _remains);
		MLUtil.writeFieldHeader(_stream, _endianness, _header);
		writeCurrentAndPrepareNextBuffer();
	}
	
	/**
	 * Check whether the end of the stream has been reached
	 */
	boolean isAtEnd()
	{
		return _buffer==null;
	}
	
	/**
	 * Skip a given number of elements
	 */
	void skip(int elements) throws IOException
	{
		int dataLength = _encodingTypeSize;
		switch(_sourceType) {
			case LOGICAL:
			case CHAR   :
			case DOUBLE :
			case SINGLE :
			case INT8   :
			case INT16  :
			case INT32  :
			case INT64  :
			case UINT8  :
			case UINT16 :
			case UINT32 :
			case UINT64 :
				for(int k=0; k<elements; ++k) {
					rawSkip(dataLength);
				}
				break;
				
			default:
				throw new MLIOException("Cannot skip in a stream of " + _sourceType + " elements");
		}
	}
	
	/**
	 * Skip a given number of elements
	 */
	void skip(int elements, Controller controller) throws IOException, Controller.CanceledByUser
	{
		int dataLength = _encodingTypeSize;
		switch(_sourceType) {
			case LOGICAL:
			case CHAR   :
			case DOUBLE :
			case SINGLE :
			case INT8   :
			case INT16  :
			case INT32  :
			case INT64  :
			case UINT8  :
			case UINT16 :
			case UINT32 :
			case UINT64 :
				for(int k=0; k<elements; ++k) {
					controller.checkPointNext();
					rawSkip(dataLength);
				}
				break;
				
			default:
				throw new MLIOException("Cannot skip in a stream of " + _sourceType + " elements");
		}
	}
	
	/**
	 * Skip one element
	 */
	private void rawSkip(int dataLength) throws IOException
	{
		_buffer.position(_buffer.position() + dataLength);
		if(_buffer.remaining()==0) {
			writeCurrentAndPrepareNextBuffer();
		}
	}
	
	/**
	 * Push a new boolean-valued element
	 */
	void pushLogical(boolean in) throws IOException
	{
		_buffer.put(in ? (byte)1 : (byte)0);
		if(_buffer.remaining()==0) {
			writeCurrentAndPrepareNextBuffer();
		}
	}
	
	/**
	 * Push a new list of boolean-valued elements
	 */
	void pushLogical(boolean[] in) throws IOException
	{
		for(int k=0; k<in.length; ++k) {
			pushLogical(in[k]);
		}
	}
	
	/**
	 * Push a new list of boolean-valued elements
	 */
	void pushLogical(boolean[] in, Controller controller) throws IOException, CanceledByUser
	{
		for(int k=0; k<in.length; ++k) {
			controller.checkPointNext();
			pushLogical(in[k]);
		}
	}
	
	/**
	 * Push a new char-valued element
	 */
	void pushChar(char in) throws IOException
	{
		_buffer.putChar(in);
		if(_buffer.remaining()==0) {
			writeCurrentAndPrepareNextBuffer();
		}
	}
	
	/**
	 * Push a new list of char-valued elements
	 */
	void pushChar(char[] in) throws IOException
	{
		for(int k=0; k<in.length; ++k) {
			pushChar(in[k]);
		}
	}
	
	/**
	 * Push a new list of char-valued elements
	 */
	void pushChar(char[] in, Controller controller) throws IOException, CanceledByUser
	{
		for(int k=0; k<in.length; ++k) {
			controller.checkPointNext();
			pushChar(in[k]);
		}
	}
	
	/**
	 * Push a new double-valued element
	 */
	void pushDouble(double in) throws IOException
	{
		_buffer.putDouble(in);
		if(_buffer.remaining()==0) {
			writeCurrentAndPrepareNextBuffer();
		}
	}
	
	/**
	 * Push a new list of double-valued elements
	 */
	void pushDouble(double[] in) throws IOException
	{
		for(int k=0; k<in.length; ++k) {
			pushDouble(in[k]);
		}
	}
	
	/**
	 * Push a new list of double-valued elements
	 */
	void pushDouble(double[] in, Controller controller) throws IOException, CanceledByUser
	{
		for(int k=0; k<in.length; ++k) {
			controller.checkPointNext();
			pushDouble(in[k]);
		}
	}
	
	/**
	 * Push a new single-valued element
	 */
	void pushSingle(float in) throws IOException
	{
		_buffer.putFloat(in);
		if(_buffer.remaining()==0) {
			writeCurrentAndPrepareNextBuffer();
		}
	}
	
	/**
	 * Push a new list of single-valued elements
	 */
	void pushSingle(float[] in) throws IOException
	{
		for(int k=0; k<in.length; ++k) {
			pushSingle(in[k]);
		}
	}
	
	/**
	 * Push a new list of single-valued elements
	 */
	void pushSingle(float[] in, Controller controller) throws IOException, CanceledByUser
	{
		for(int k=0; k<in.length; ++k) {
			controller.checkPointNext();
			pushSingle(in[k]);
		}
	}
	
	/**
	 * Push a new int8-valued element
	 */
	void pushInt8(byte in) throws IOException
	{
		_buffer.put(in);
		if(_buffer.remaining()==0) {
			writeCurrentAndPrepareNextBuffer();
		}
	}
	
	/**
	 * Push a new list of int8-valued elements
	 */
	void pushInt8(byte[] in) throws IOException
	{
		for(int k=0; k<in.length; ++k) {
			pushInt8(in[k]);
		}
	}
	
	/**
	 * Push a new list of int8-valued elements
	 */
	void pushInt8(byte[] in, Controller controller) throws IOException, CanceledByUser
	{
		for(int k=0; k<in.length; ++k) {
			controller.checkPointNext();
			pushInt8(in[k]);
		}
	}
	
	/**
	 * Push a new int16-valued element
	 */
	void pushInt16(short in) throws IOException
	{
		_buffer.putShort(in);
		if(_buffer.remaining()==0) {
			writeCurrentAndPrepareNextBuffer();
		}
	}
	
	/**
	 * Push a new list of int16-valued elements
	 */
	void pushInt16(short[] in) throws IOException
	{
		for(int k=0; k<in.length; ++k) {
			pushInt16(in[k]);
		}
	}
	
	/**
	 * Push a new list of int16-valued elements
	 */
	void pushInt16(short[] in, Controller controller) throws IOException, CanceledByUser
	{
		for(int k=0; k<in.length; ++k) {
			controller.checkPointNext();
			pushInt16(in[k]);
		}
	}
	
	/**
	 * Push a new int32-valued element
	 */
	void pushInt32(int in) throws IOException
	{
		_buffer.putInt(in);
		if(_buffer.remaining()==0) {
			writeCurrentAndPrepareNextBuffer();
		}
	}
	
	/**
	 * Push a new list of int32-valued elements
	 */
	void pushInt32(int[] in) throws IOException
	{
		for(int k=0; k<in.length; ++k) {
			pushInt32(in[k]);
		}
	}
	
	/**
	 * Push a new list of int32-valued elements
	 */
	void pushInt32(int[] in, Controller controller) throws IOException, CanceledByUser
	{
		for(int k=0; k<in.length; ++k) {
			controller.checkPointNext();
			pushInt32(in[k]);
		}
	}
	
	/**
	 * Push a new int64-valued element
	 */
	void pushInt64(long in) throws IOException
	{
		_buffer.putLong(in);
		if(_buffer.remaining()==0) {
			writeCurrentAndPrepareNextBuffer();
		}
	}
	
	/**
	 * Push a new list of int64-valued elements
	 */
	void pushInt64(long[] in) throws IOException
	{
		for(int k=0; k<in.length; ++k) {
			pushInt64(in[k]);
		}
	}
	
	/**
	 * Push a new list of int64-valued elements
	 */
	void pushInt64(long[] in, Controller controller) throws IOException, CanceledByUser
	{
		for(int k=0; k<in.length; ++k) {
			controller.checkPointNext();
			pushInt64(in[k]);
		}
	}
	
	/**
	 * Push a new uint8-valued element
	 */
	void pushUInt8(byte in) throws IOException
	{
		_buffer.put(in);
		if(_buffer.remaining()==0) {
			writeCurrentAndPrepareNextBuffer();
		}
	}
	
	/**
	 * Push a new list of uint8-valued elements
	 */
	void pushUInt8(byte[] in) throws IOException
	{
		for(int k=0; k<in.length; ++k) {
			pushUInt8(in[k]);
		}
	}
	
	/**
	 * Push a new list of uint8-valued elements
	 */
	void pushUInt8(byte[] in, Controller controller) throws IOException, CanceledByUser
	{
		for(int k=0; k<in.length; ++k) {
			controller.checkPointNext();
			pushUInt8(in[k]);
		}
	}
	
	/**
	 * Push a new uint16-valued element
	 */
	void pushUInt16(short in) throws IOException
	{
		_buffer.putShort(in);
		if(_buffer.remaining()==0) {
			writeCurrentAndPrepareNextBuffer();
		}
	}
	
	/**
	 * Push a new list of uint16-valued elements
	 */
	void pushUInt16(short[] in) throws IOException
	{
		for(int k=0; k<in.length; ++k) {
			pushUInt16(in[k]);
		}
	}
	
	/**
	 * Push a new list of uint16-valued elements
	 */
	void pushUInt16(short[] in, Controller controller) throws IOException, CanceledByUser
	{
		for(int k=0; k<in.length; ++k) {
			controller.checkPointNext();
			pushUInt16(in[k]);
		}
	}
	
	/**
	 * Push a new uint32-valued element
	 */
	void pushUInt32(int in) throws IOException
	{
		_buffer.putInt(in);
		if(_buffer.remaining()==0) {
			writeCurrentAndPrepareNextBuffer();
		}
	}
	
	/**
	 * Push a new list of uint32-valued elements
	 */
	void pushUInt32(int[] in) throws IOException
	{
		for(int k=0; k<in.length; ++k) {
			pushUInt32(in[k]);
		}
	}
	
	/**
	 * Push a new list of uint32-valued elements
	 */
	void pushUInt32(int[] in, Controller controller) throws IOException, CanceledByUser
	{
		for(int k=0; k<in.length; ++k) {
			controller.checkPointNext();
			pushUInt32(in[k]);
		}
	}
	
	/**
	 * Push a new uint64-valued element
	 */
	void pushUInt64(long in) throws IOException
	{
		_buffer.putLong(in);
		if(_buffer.remaining()==0) {
			writeCurrentAndPrepareNextBuffer();
		}
	}
	
	/**
	 * Push a new list of uint64-valued elements
	 */
	void pushUInt64(long[] in) throws IOException
	{
		for(int k=0; k<in.length; ++k) {
			pushUInt64(in[k]);
		}
	}
	
	/**
	 * Push a new list of uint64-valued elements
	 */
	void pushUInt64(long[] in, Controller controller) throws IOException, CanceledByUser
	{
		for(int k=0; k<in.length; ++k) {
			controller.checkPointNext();
			pushUInt64(in[k]);
		}
	}
	
	/**
	 * Write the current buffer and prepare the next one
	 */
	private void writeCurrentAndPrepareNextBuffer() throws IOException
	{
		// Write the current buffer
		if(_buffer!=null) {
			MLUtil.push(_stream, _buffer);
			_remains -= _buffer.limit();
		}
		
		// Prepare the next buffer, or write the padding bytes if the end of the
		// stream have been reached
		if(_remains==0) {
			_buffer = null;
			MLUtil.skip(_stream, _header.getPaddingLength());
		}
		else {
			int newBufferLength = Math.min(CHUNK_SIZE, _remains);
			_buffer = MLUtil.allocate(_endianness, newBufferLength);
		}
	}
}
