package plugins.ylemontag.matlabio.lib;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Type of a field in a .mat file (correspond to constants miFOO in the Matlab .mat file reference) 
 */
public enum MLRawType
{
	INT8      ((byte) 1,  1, "int8"      ),
	UINT8     ((byte) 2,  1, "uint8"     ),
	INT16     ((byte) 3,  2, "int16"     ),
	UINT16    ((byte) 4,  2, "uint16"    ),
	INT32     ((byte) 5,  4, "int32"     ),
	UINT32    ((byte) 6,  4, "uint32"    ),
	INT64     ((byte)12,  8, "int64"     ),
	UINT64    ((byte)13,  8, "uint64"    ),
	SINGLE    ((byte) 7,  4, "single"    ),
	DOUBLE    ((byte) 9,  8, "double"    ),
	MATRIX    ((byte)14, -1, "matrix"    ),
	COMPRESSED((byte)15, -1, "compressed"),
	UTF8      ((byte)16, -1, "utf8"      ),
	UTF16     ((byte)17,  2, "utf16"     ),
	UTF32     ((byte)18,  4, "utf32"     );
	
	private byte   _code;
	private int    _size;
	private String _name;
	
	/**
	 * Convert a numeric value read from a file into a MLRawType object
	 */
	static MLRawType byteToMLRawType(byte value) throws MLIOException
	{
		switch(value) {
			case  1: return INT8      ;
			case  2: return UINT8     ;
			case  3: return INT16     ;
			case  4: return UINT16    ;
			case  5: return INT32     ;
			case  6: return UINT32    ;
			case 12: return INT64     ;
			case 13: return UINT64    ;
			case  7: return SINGLE    ;
			case  9: return DOUBLE    ;
			case 14: return MATRIX    ;
			case 15: return COMPRESSED;
			case 16: return UTF8      ;
			case 17: return UTF16     ;
			case 18: return UTF32     ;
			default:
				throw new MLIOException("Unknown MLRawType code: " + value);
		}
	}
	
	/**
	 * Constructor
	 */
	private MLRawType(byte code, int size, String name)
	{
		_code = code;
		_size = size;
		_name = name;
	}
	
	/**
	 * Code used to flag the field type in Matlab .mat files
	 */
	byte getCode()
	{
		return _code;
	}
	
	/**
	 * Size (in bytes) of an element of this type
	 * @warning Throw an exception for non-atomic types
	 */
	int getSize() throws MLIOException
	{
		if(_size<0) {
			throw new MLIOException("Cannot determine the size of an element of type " + _name);
		}
		return _size;
	}
	
	@Override
	public String toString()
	{
		return _name;
	}
}
