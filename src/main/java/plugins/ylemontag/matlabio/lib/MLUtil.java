package plugins.ylemontag.matlabio.lib;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Low-level tools for reading/writing a Matlab .mat file
 */
class MLUtil
{	
	/**
	 * Exception thrown when the end of an input stream is reached
	 */
	static class EndOfStream extends MLIOException
	{
		private static final long serialVersionUID = 1L;
		
		/**
		 * Constructor
		 */
		EndOfStream()
		{
			super("EOF reached unexpectedly");
		}
	}
	
	
	
	/**
	 * Header of an element field in a .mat file
	 */
	static class FieldHeader
	{
		private MLRawType _type;
		private boolean _useShortFormat;
		private int _headerLength ;
		private int _dataLength   ;
		private int _paddingLength;
		
		/**
		 * Default constructor (use either short format or long format according to
		 * the length of the data)
		 */
		FieldHeader(MLRawType type, int dataLength)
		{
			this(type, dataLength, dataLength<=4);
		}
		
		/**
		 * Constructor
		 */
		FieldHeader(MLRawType type, int dataLength, boolean useShortFormat)
		{
			_type           = type;
			_useShortFormat = useShortFormat;
			_headerLength  = useShortFormat ? 4 : 8;
			_dataLength    = dataLength;
			_paddingLength = 8 - ((_headerLength+_dataLength) % 8);
			if(_paddingLength==8) {
				_paddingLength = 0;
			}
		}
		
		/**
		 * Underlying raw-type
		 */
		MLRawType getType()
		{
			return _type; 
		}
		
		/**
		 * Flag indicating whether the field use a short-format encoding or not
		 */
		boolean getUseShortFormat()
		{
			return _useShortFormat;
		}
		
		/**
		 * Length of the header section of the field
		 */
		int getHeaderLength()
		{
			return _headerLength;
		}
		
		/**
		 * Length of the data section of the field
		 */
		int getDataLength()
		{
			return _dataLength;
		}
		
		/**
		 * Length of the padding section at the end of the field
		 */
		int getPaddingLength()
		{
			return _paddingLength;
		}
		
		/**
		 * Total length of the field, including the header, the data and the padding section
		 */
		int getTotalLength()
		{
			return _headerLength + _dataLength + _paddingLength;
		}
	}
	
	
	
	/**
	 * Field of an element in a .mat file
	 */
	static class Field extends FieldHeader
	{	
		private ByteBuffer _data;
		
		/**
		 * Default constructor (use either short format or long format according to
		 * the length of the data)
		 */
		Field(MLRawType type, ByteBuffer data)
		{
			super(type, data.limit());
			_data = data;
		}
		
		/**
		 * Constructor
		 */
		Field(FieldHeader header, ByteBuffer data)
		{
			super(header.getType(), header.getDataLength(), header.getUseShortFormat());
			_data = data;
		}
		
		/**
		 * Field data
		 */
		ByteBuffer getData()
		{
			return _data;
		}
	}
	
	
	
	/**
	 * Read a field
	 */
	static Field readField(InputStream stream, ByteOrder endianness) throws IOException
	{
		FieldHeader header = readFieldHeader(stream, endianness);
		ByteBuffer data = consume(stream, endianness, header.getDataLength());
		consume(stream, endianness, header.getPaddingLength());
		return new Field(header, data);
	}
	
	/**
	 * Write the given field to the stream
	 */
	static void writeField(OutputStream stream, ByteOrder endianness, Field field) throws IOException
	{
		writeFieldHeader(stream, endianness, field);
		push(stream, field.getData());
		skip(stream, field.getPaddingLength());
	}
	
	/**
	 * Read the header of a field
	 */
	static FieldHeader readFieldHeader(InputStream stream, ByteOrder endianness) throws IOException
	{
		int rawTypeCode = consume(stream, endianness, 4).getInt();
		MLRawType rawType;
		int dataLength;
		boolean shortFormat;
		if((rawTypeCode & 0xffff0000)==0) { // Long field format
			rawType     = MLRawType.byteToMLRawType((byte)rawTypeCode);
			dataLength  = consume(stream, endianness, 4).getInt();
			shortFormat = false;
		}
		else { // Short field format
			rawType     = MLRawType.byteToMLRawType((byte)(rawTypeCode & 0x0000ffff));
			dataLength  = (rawTypeCode >> 16);
			shortFormat = true;
		}
		if(dataLength<0) {
			throw new MLIOException("Negative field length detected");
		}
		return new FieldHeader(rawType, dataLength, shortFormat);
	}
	
	/**
	 * Append the given field header to the stream
	 */
	static void writeFieldHeader(OutputStream stream, ByteOrder endianness, FieldHeader fieldHeader)
		throws IOException
	{
		ByteBuffer buffer;
		if(fieldHeader.getHeaderLength()==4) { // Short field format
			int rawType = fieldHeader.getType().getCode();
			rawType    |= (fieldHeader.getDataLength() << 16);
			buffer      = allocate(endianness, 4);
			buffer.putInt(rawType);
		}
		else if(fieldHeader.getHeaderLength()==8) { // Long field format
			buffer = allocate(endianness, 8);
			buffer.putInt(fieldHeader.getType().getCode());
			buffer.putInt(fieldHeader.getDataLength());
		}
		else {
			throw new MLIOException("Field header length must be either 4 or 8 byte long");
		}
		push(stream, buffer);
	}
	
	/**
	 * Read 'nb' bytes from the given stream
	 */
	static ByteBuffer consume(InputStream stream, ByteOrder endianness, int length) throws IOException
	{
		ByteBuffer retVal = allocate(endianness, length);
		consume(stream, retVal, 0, length);
		return retVal;
	}
	
	/**
	 * Read 'nb' bytes from the given stream and put it in the given buffer
	 */
	static void consume(InputStream stream, ByteBuffer buffer, int offset, int length) throws IOException
	{
		byte[] rawBuffer = buffer.array();
		int finalOffset = offset + length;
		if(finalOffset>rawBuffer.length) {
			throw new MLIOException("Unsufficent remaining space in the buffer");
		}
		while(offset<finalOffset) {
			int currentlyRead = stream.read(rawBuffer, offset, finalOffset - offset);
			if(currentlyRead>=0) {
				offset += currentlyRead;
			}
			else {
				break;
			}
		}
		if(offset<finalOffset) {
			throw new EndOfStream();
		}
	}
	
	/**
	 * Copy 'length' bytes from in to out
	 * @throw IOException If either out.remaining()<length or in.remaining()<length
	 */
	static void copy(ByteBuffer out, int outOffset, ByteBuffer in, int inOffset, int length) throws MLIOException
	{
		if(length==0) {
			return;
		}
		byte[] rawOut = out.array();
		byte[] rawIn  = in .array();
		int finalOutOffset = outOffset + length;
		int finalInOffset  = inOffset  + length;
		if(finalOutOffset>rawOut.length) {
			throw new MLIOException("Overflow in the output buffer");
		}
		if(finalInOffset>rawIn.length) {
			throw new MLIOException("Overflow in the input buffer");
		}
		for(; outOffset<finalOutOffset; ++outOffset) {
			rawOut[outOffset] = rawIn[inOffset];
			++inOffset;
		}
	}
	
	/**
	 * Allocate a new buffer with the given size and endianness
	 */
	static ByteBuffer allocate(ByteOrder endianness, int length)
	{
		byte[] buffer = new byte[length];
		ByteBuffer retVal = ByteBuffer.wrap(buffer);
		retVal.order(endianness);
		return retVal;
	}
	
	/**
	 * Write a buffer of bytes in the given stream
	 */
	static void push(OutputStream stream, ByteBuffer buffer) throws IOException
	{
		stream.write(buffer.array());
	}
	
	/**
	 * Write a buffer of bytes in the given random access file
	 */
	static void push(RandomAccessFile stream, ByteBuffer buffer) throws IOException
	{
		stream.write(buffer.array());
	}
	
	/**
	 * Write padding data in the given stream
	 */
	static void skip(OutputStream stream, int length) throws IOException
	{
		if(length>0) {
			stream.write(new byte[length]);
		}
	}
}
