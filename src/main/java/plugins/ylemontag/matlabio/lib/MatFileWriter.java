package plugins.ylemontag.matlabio.lib;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.lang.ref.WeakReference;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.zip.DeflaterOutputStream;

import plugins.ylemontag.matlabio.lib.Controller.CanceledByUser;

/**
 * 
 * @author Yoann Le Montagner
 *
 * Matlab .mat file writer
 */
public class MatFileWriter
{
	private File _file;
	private boolean _useCompression;
	private ByteOrder _endianness;
	private RandomAccessFile _mainStream;
	private SectionValidator _currentValidator;
	private WeakReference<SectionOutputStream> _currentSection;
	private Map<String, MLMeta> _index;
	
	/**
	 * Open a .mat file for writing
	 */
	public MatFileWriter(String path, boolean append) throws IOException
	{
		this(new File(path), append);
	}
	
	/**
	 * Open a .mat file for writing
	 */
	public MatFileWriter(File file, boolean append) throws IOException
	{
		// Initialize objects
		if(file==null) {
			throw new MLIOException("No file selected");
		}
		_file = file;
		_useCompression = false;
		_index = new HashMap<String, MLMeta>();
		
		// Try reading the existing file in case of append==true
		if(append) {
			MatFileReader reader = new MatFileReader(file);
			_endianness = reader.getEndianness();
			for(String key : reader.getKeys()) {
				_index.put(key, reader.getMeta(key));
			}
		}
		else {
			_endianness = ByteOrder.LITTLE_ENDIAN;
		}
		
		// Create the output stream
		_mainStream = new RandomAccessFile(_file, "rw");
		if(append) {
			_mainStream.seek(_mainStream.length());
		}
		else {
			_mainStream.setLength(0);
			writeHeader();
		}
	}
	
	/**
	 * Return the underlying file
	 */
	public File getFile()
	{
		return _file;
	}
	
	/**
	 * Return the endianness used for this file
	 */
	public ByteOrder getEndianness()
	{
		return _endianness;
	}
	
	/**
	 * List of the variables contained in the Matlab .mat file
	 */
	public Set<String> getKeys()
	{
		return _index.keySet();
	}
	
	/**
	 * Return the meta-data associated to a given variable
	 * @return null if the key does not exist
	 */
	public MLMeta getMeta(String key)
	{
		return _index.get(key);
	}
	
	/**
	 * Append an array to the end of the file
	 */
	public void putData(MLArray array) throws IOException
	{
		try {
			putData(array, new Controller());
		}
		catch(CanceledByUser err) {
			throw new MLIOException("A Matlab array export operation have been unexpectedly interrupted");
		}
	}
	
	/**
	 * Append an array to the end of the file
	 */
	public void putData(MLArray array, Controller controller) throws IOException, Controller.CanceledByUser
	{
		OutputStream rawStream = allocateStream(array.getMeta());
		processArray(rawStream, array, controller);
		rawStream.close();
	}
	
	/**
	 * Open a stream to write data and the end of the file
	 */
	public MLOStream putDataAsStream(MLMeta meta) throws IOException
	{
		OutputStream rawStream = allocateStream(meta);
		return processStream(rawStream, meta);
	}
	
	/**
	 * Writing the file header
	 */
	private void writeHeader() throws IOException
	{
		ByteBuffer buffer = MLUtil.allocate(_endianness, 128);
		
		// Write the description field
		String description = "MATLAB 5.0 MAT-file, Created by: MatlabIO java library";
		for(char c : description.toCharArray()) {
			buffer.put((byte)c);
		}
		for(int k=description.length(); k<124; ++k) {
			buffer.put((byte)' ');
		}
		
		// Endianness indicator and version flag
		if(_endianness==ByteOrder.LITTLE_ENDIAN) {
			buffer.put(124, (byte)0x00);
			buffer.put(125, (byte)0x01);
			buffer.put(126, (byte)'I');
			buffer.put(127, (byte)'M');
		}
		else if(_endianness==ByteOrder.BIG_ENDIAN) {
			buffer.put(124, (byte)0x01);
			buffer.put(125, (byte)0x00);
			buffer.put(126, (byte)'M');
			buffer.put(127, (byte)'I');
		}
		else {
			throw new MLIOException("Unknown endianness flag: " + _endianness);
		}
		
		// Actually write the file header
		MLUtil.push(_mainStream, buffer);
	}
	
	/**
	 * Allocate a new stream to write a new element
	 */
	private OutputStream allocateStream(MLMeta meta) throws IOException
	{
		// Ensure that no section is beeing written
		ensurePreviousSectionClosed();
		
		// Check that the name of the new element is not already used in the file
		if(_index.containsKey(meta.getName())) {
			throw new MLIOException(
				"The key '" + meta.getName() + "' is already in used in " + _file.getName()
			);
		}
		
		// Allocate the new section validator and output stream
		_currentValidator = new SectionValidator(meta);
		SectionOutputStream currentSection = new SectionOutputStream(_mainStream, _currentValidator);
		_currentSection = new WeakReference<SectionOutputStream>(currentSection);
		
		// With the compressed option
		if(_useCompression) {
			OutputStream retVal = new DeflaterOutputStream(currentSection);
			ByteBuffer compressedHeader = MLUtil.allocate(_endianness, 8);
			compressedHeader.putInt(MLRawType.MATRIX.getCode());
			compressedHeader.putInt(0); // This would lead to errors, therefore compression should not be used
			MLUtil.push(retVal, compressedHeader);
			return retVal;
		}
		
		// Without the compressed option
		else {
			return currentSection;
		}
	}
	
	@Override
	protected void finalize() throws Throwable
	{
		ensurePreviousSectionClosed();
	}
	
	/**
	 * Ensure that the previously written section either has been properly closed
	 * or has been interrupted
	 */
	private void ensurePreviousSectionClosed() throws IOException
	{
		if(_currentValidator==null || _currentValidator.isClosed()) {
			return;
		}
		
		// Try to release the weak reference pointing on the output stream used to
		// write the current section
		System.gc();
		
		// If the weak reference cannot be released, this means that a write operation
		// is in progress
		if(_currentSection!=null && _currentSection.get()!=null) {
			throw new MLIOException("There is still a write operation in progress");
		}
		
		// Otherwise, this means that the previous write operation have been canceled,
		// so force the section to be closed
		else {
			_currentValidator.safeClose();
			_currentValidator = null;
			_currentSection   = null;
		}
	}
	
	/**
	 * Write the meta-data associated to an array in the given stream
	 */
	private void processMeta(OutputStream stream, MLMeta meta) throws IOException
	{
		ByteBuffer buffer;
		
		// Array flags sub-element
		int rawFlags = meta.getType().getCode();
		if(meta.getType()==MLType.LOGICAL) {
			rawFlags |= 0x00000200;
		}
		if(meta.getIsComplex()) {
			rawFlags |= 0x00000800;
		}
		buffer = MLUtil.allocate(_endianness, 8);
		buffer.putInt(rawFlags);
		MLUtil.writeField(stream, _endianness, new MLUtil.Field(MLRawType.UINT32, buffer));
		
		// Dimensions sub-element
		int[] dims = meta.getDimensions();
		buffer = MLUtil.allocate(_endianness, 4*dims.length);
		for(int k=0; k<dims.length; ++k) {
			buffer.putInt(dims[k]);
		}
		MLUtil.writeField(stream, _endianness, new MLUtil.Field(MLRawType.INT32, buffer));
		
		// Name sub-element
		String name = meta.getName();
		buffer = MLUtil.allocate(_endianness, name.length());
		for(int k=0; k<name.length(); ++k) {
			buffer.put((byte)name.charAt(k));
		}
		MLUtil.writeField(stream, _endianness, new MLUtil.Field(MLRawType.INT8, buffer));
	}
	
	/**
	 * Write an array in the given stream
	 */
	private void processArray(OutputStream stream, MLArray array, Controller controller)
		throws IOException, Controller.CanceledByUser
	{
		processMeta(stream, array.getMeta());
		MLType type = array.getType();
		switch(type) {
			case INT8  :
			case UINT8 :
			case INT16 :
			case UINT16:
			case INT32 :
			case UINT32:
			case INT64 :
			case UINT64:
			case SINGLE:
			case DOUBLE:
				processNumericArray(stream, (MLArrays.Numeric<?>)array, controller);
				break;
			case CHAR:
				processCharArray(stream, (MLArrays.Char)array, controller);
				break;
			case LOGICAL:
				processLogicalArray(stream, (MLArrays.Logical)array, controller);
				break;
			default:
				throw new MLIOException("Export of " + type + " objects not implemented yet");
		}
	}
	
	/**
	 * Write the content of a numeric array in the given stream
	 */
	private void processNumericArray(OutputStream stream, MLArrays.Numeric<?> array, Controller controller)
		throws IOException, Controller.CanceledByUser
	{
		MLType  type      = array.getType();
		boolean isComplex = array.getIsComplex();
		int     elements  = array.getSize();
		
		// Flush the data
		controller.startCounter(isComplex ? elements*2 : elements);
		MLDataOutputStream realPart = new MLDataOutputStream(stream, _endianness, type, elements);
		flushNumericArray(realPart, array, true, controller);
		if(isComplex) {
			MLDataOutputStream imaginaryPart = new MLDataOutputStream(stream, _endianness, type, elements);
			flushNumericArray(imaginaryPart, array, false, controller);
		}
		controller.stopCounter();
	}
	
	/**
	 * Flush the content of a MLArray into a MLDataOutputStream
	 */
	private void flushNumericArray(MLDataOutputStream dataStream, MLArrays.Numeric<?> source, boolean isRealPart,
		Controller controller) throws IOException, Controller.CanceledByUser
	{
		if(isRealPart) {
			switch(source.getType()) {
				case INT8  : dataStream.pushInt8  (((MLArrays.Int8  )source).getReal(), controller); break;
				case UINT8 : dataStream.pushUInt8 (((MLArrays.UInt8 )source).getReal(), controller); break;
				case INT16 : dataStream.pushInt16 (((MLArrays.Int16 )source).getReal(), controller); break;
				case UINT16: dataStream.pushUInt16(((MLArrays.UInt16)source).getReal(), controller); break;
				case INT32 : dataStream.pushInt32 (((MLArrays.Int32 )source).getReal(), controller); break;
				case UINT32: dataStream.pushUInt32(((MLArrays.UInt32)source).getReal(), controller); break;
				case INT64 : dataStream.pushInt64 (((MLArrays.Int64 )source).getReal(), controller); break;
				case UINT64: dataStream.pushUInt64(((MLArrays.UInt64)source).getReal(), controller); break;
				case SINGLE: dataStream.pushSingle(((MLArrays.Single)source).getReal(), controller); break;
				case DOUBLE: dataStream.pushDouble(((MLArrays.Double)source).getReal(), controller); break;
				default:
					throw new MLIOException("Cannot process a numeric array with a " + source.getType() + " type");
			}
		}
		else {
			switch(source.getType()) {
				case INT8  : dataStream.pushInt8  (((MLArrays.Int8  )source).getImaginary(), controller); break;
				case UINT8 : dataStream.pushUInt8 (((MLArrays.UInt8 )source).getImaginary(), controller); break;
				case INT16 : dataStream.pushInt16 (((MLArrays.Int16 )source).getImaginary(), controller); break;
				case UINT16: dataStream.pushUInt16(((MLArrays.UInt16)source).getImaginary(), controller); break;
				case INT32 : dataStream.pushInt32 (((MLArrays.Int32 )source).getImaginary(), controller); break;
				case UINT32: dataStream.pushUInt32(((MLArrays.UInt32)source).getImaginary(), controller); break;
				case INT64 : dataStream.pushInt64 (((MLArrays.Int64 )source).getImaginary(), controller); break;
				case UINT64: dataStream.pushUInt64(((MLArrays.UInt64)source).getImaginary(), controller); break;
				case SINGLE: dataStream.pushSingle(((MLArrays.Single)source).getImaginary(), controller); break;
				case DOUBLE: dataStream.pushDouble(((MLArrays.Double)source).getImaginary(), controller); break;
				default:
					throw new MLIOException("Cannot process a numeric array with a " + source.getType() + " type");
			}
		}
	}
	
	/**
	 * Write the content of a char array in the given stream
	 */
	private void processCharArray(OutputStream stream, MLArrays.Char array, Controller controller)
		throws IOException, Controller.CanceledByUser
	{
		int elements = array.getSize();
		MLDataOutputStream dataStream = new MLDataOutputStream(stream, _endianness, MLType.CHAR, elements);
		controller.startCounter(elements);
		dataStream.pushChar(array.get(), controller);
		controller.stopCounter();
	}
	
	/**
	 * Write the content of a boolean array in the given stream
	 */
	private void processLogicalArray(OutputStream stream, MLArrays.Logical array, Controller controller)
		throws IOException, Controller.CanceledByUser
	{
		int elements = array.getSize();
		MLDataOutputStream dataStream = new MLDataOutputStream(stream, _endianness, MLType.LOGICAL, elements);
		controller.startCounter(elements);
		dataStream.pushLogical(array.get(), controller);
		controller.stopCounter();
	}
	
	/**
	 * Prepare a stream object to write at the end of the current file 
	 */
	private MLOStream processStream(OutputStream stream, MLMeta meta) throws IOException
	{
		processMeta(stream, meta);
		MLType type = meta.getType();
		switch(type) {
			case INT8  : return new MLOStreams.Int8  (meta, stream, _endianness);
			case UINT8 : return new MLOStreams.UInt8 (meta, stream, _endianness);
			case INT16 : return new MLOStreams.Int16 (meta, stream, _endianness);
			case UINT16: return new MLOStreams.UInt16(meta, stream, _endianness);
			case INT32 : return new MLOStreams.Int32 (meta, stream, _endianness);
			case UINT32: return new MLOStreams.UInt32(meta, stream, _endianness);
			case INT64 : return new MLOStreams.Int64 (meta, stream, _endianness);
			case UINT64: return new MLOStreams.UInt64(meta, stream, _endianness);
			case SINGLE: return new MLOStreams.Single(meta, stream, _endianness);
			case DOUBLE: return new MLOStreams.Double(meta, stream, _endianness);
			default:
				throw new MLIOException("Export of " + type + " objects not implemented yet");
		}
	}
	
	/**
	 * Hold the necessary information to close a section in a Matlab .mat file in
	 * a proper way
	 */
	private class SectionValidator
	{
		private MLMeta  _meta;
		private long    _initialPosition;
		private boolean _validated;
		private boolean _closed;
		
		/**
		 * Constructor
		 */
		public SectionValidator(MLMeta meta) throws IOException
		{
			_meta            = meta;
			_initialPosition = _mainStream.getFilePointer();
			_validated       = false;
			_closed          = false;
			_mainStream.seek(_initialPosition + 8);
		}
		
		/**
		 * Check if the section has been closed
		 */
		public boolean isClosed()
		{
			return _closed;
		}
		
		/**
		 * Mark the section as valid, and close it
		 */
		public void validateAndClose() throws IOException
		{
			_validated = true;
			safeClose();
		}
		
		/**
		 * Close the section, i.e. report its length if it has been marked as valid,
		 * or cut the file at its begining if it has not 
		 */
		public void safeClose() throws IOException
		{
			if(_closed) {
				return;
			}
			
			// Report the length and type of the section at its begining if it has
			// been marked as valid
			if(_validated)
			{
				// Determinate the size of the current element in the file
				long currentPosition = _mainStream.getFilePointer();
				int actualElementSize = (int)(currentPosition - _initialPosition) - 8;
				ByteBuffer elementHeader = MLUtil.allocate(_endianness, 8);
				elementHeader.putInt(_useCompression ? MLRawType.COMPRESSED.getCode() : MLRawType.MATRIX.getCode());
				elementHeader.putInt(actualElementSize);
				
				// Write it at its beginning and go back to the current position 
				_mainStream.seek(_initialPosition);
				MLUtil.push(_mainStream, elementHeader);
				_mainStream.seek(currentPosition);
				
				// Register the variable just written in the local index
				_index.put(_meta.getName(), _meta);
			}
			
			// Otherwise, cut the file
			else {
				_mainStream.setLength(_initialPosition);
			}
			
			// Now, the section is closed
			_closed = true;
		}
	}
	
	/**
	 * Useful class to keep track of the number of written bytes
	 */
	private static class SectionOutputStream extends OutputStream
	{
		private RandomAccessFile _out;
		private SectionValidator _validator;
		
		/**
		 * Constructor
		 */
		public SectionOutputStream(RandomAccessFile out, SectionValidator validator)
		{
			_out       = out;
			_validator = validator;
		}
		
		@Override
		public void finalize() throws Throwable {}
		
		@Override
		public void close() throws IOException
		{
			if(_validator!=null) {
				_validator.validateAndClose();
				_validator = null;
			}
		}
		
		@Override
		public void write(byte[] b, int off, int len) throws IOException
		{
			_out.write(b, off, len);
		}
		
		@Override
		public void write(int b) throws IOException
		{
			_out.write(b);
		}
	}
}
