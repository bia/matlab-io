package plugins.ylemontag.matlabio;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginLibrary;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Entry point for the library
 */
public class MatlabIOLibrary extends Plugin implements PluginLibrary
{
	// That's all!
}
