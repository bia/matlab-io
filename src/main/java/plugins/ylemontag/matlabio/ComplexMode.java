package plugins.ylemontag.matlabio;

/**
 * 
 * @author Yoann Le Montagner
 *
 * Import option for complex valued Matlab arrays
 */
public enum ComplexMode
{
	REAL_PART     ("Real"     ),
	IMAGINARY_PART("Imaginary"),
	BOTH          ("Both"     );
	
	private String _description;
	
	/**
	 * Constructor
	 */
	private ComplexMode(String description)
	{
		_description = description;
	}
	
	/**
	 * Description
	 */
	public String getDescription()
	{
		return _description;
	}
	
	@Override
	public String toString()
	{
		return getDescription();
	}
	
	/**
	 * Return the complex mode corresponding to the given description, or null
	 * if no mode corresponds
	 */
	public static ComplexMode fromDescription(String description)
	{
		for(ComplexMode c : values()) {
			if(c.getDescription().equals(description)) {
				return c;
			}
		}
		return null;
	}
}
