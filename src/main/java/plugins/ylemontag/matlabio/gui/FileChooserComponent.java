package plugins.ylemontag.matlabio.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.LinkedList;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Component used to select a .mat file
 */
public class FileChooserComponent extends JButton
{	
	private static final long serialVersionUID = 1L;

	/**
	 * Interface to implement in order to listen to file changes 
	 */
	public interface FileChangedListener
	{	
		/**
		 * Action fired when a file change occur
		 */
		public void actionPerformed(File newFile);
	}
	
	/**
	 * File chooser dialog mode
	 */
	public static enum Mode
	{
		OPEN_DIALOG,
		SAVE_DIALOG
	}
	
	private Mode _mode;
	private File _file;
	private JFileChooser _fileChooser;
	private LinkedList<FileChangedListener> _listeners;
	
	/**
	 * Constructor
	 */
	public FileChooserComponent(Mode mode, FileNameExtensionFilter filter)
	{
		super();
		refreshButtonLabel();
		_listeners = new LinkedList<FileChangedListener>();
		
		// File chooser
		_mode = mode;
		_fileChooser = new JFileChooser();
		_fileChooser.setFileFilter(filter);
		
		// Listener
		addActionListener(new ActionListener()
		{	
			@Override
			public void actionPerformed(ActionEvent e) {
				onButtonClicked();
			}
		});
	}
	
	/**
	 * Current directory
	 */
	public File getCurrentDirectory()
	{
		return _fileChooser.getCurrentDirectory();
	}
	
	/**
	 * Change the current directory
	 */
	public void setCurrentDirectory(File newDirectory)
	{
		_fileChooser.setCurrentDirectory(newDirectory);
	}
	
	/**
	 * Selected file
	 */
	public File getSelectedFile()
	{
		return _file;
	}
	
	/**
	 * Change the selected file
	 */
	public void setSelectedFile(File newFile)
	{
		if(_file==null && newFile==null) {
			return;
		}
		else if(_file!=null && newFile!=null && _file.equals(newFile)) {
			return;
		}
		_file = newFile;
		refreshButtonLabel();
		for(FileChangedListener l : _listeners) {
			l.actionPerformed(_file);
		}
	}
	
	/**
	 * Listen to the file change events
	 */
	public void addFileChangedListener(FileChangedListener l)
	{
		_listeners.add(l);
	}
	
	/**
	 * Action performed when the user click on the button
	 */
	private void onButtonClicked()
	{
		if(_file!=null) {
			_fileChooser.setSelectedFile(_file);
		}
		int retVal = 0;
		switch(_mode) {
			case OPEN_DIALOG: retVal = _fileChooser.showOpenDialog(this); break;
			case SAVE_DIALOG: retVal = _fileChooser.showSaveDialog(this); break;
		}
		if(retVal==JFileChooser.APPROVE_OPTION) {
			setSelectedFile(_fileChooser.getSelectedFile());
		}
	}
	
	/**
	 * Refresh the file name on the button label
	 */
	private void refreshButtonLabel()
	{
		String newLabel = _file==null ? "Select a file..." : _file.getName();
		setText(newLabel);
	}
}
