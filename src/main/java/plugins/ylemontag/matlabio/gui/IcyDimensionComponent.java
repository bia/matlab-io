package plugins.ylemontag.matlabio.gui;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;

import plugins.ylemontag.matlabio.IcyDimension;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Component letting the user choose one dimension among the 5 dimensions
 * defined by the ICY API
 */
public class IcyDimensionComponent extends JComboBox {

	private static final long serialVersionUID = 1L;

	private DefaultComboBoxModel _model;
	
	/**
	 * Constructor
	 */
	public IcyDimensionComponent() {
		super();
		_model = new DefaultComboBoxModel();
		setModel(_model);
		feedModel();
	}
	
	/**
	 * Selected dimension
	 */
	public IcyDimension getIcyDimension() {
		return (IcyDimension)getSelectedItem();
	}
	
	/**
	 * Change the selected dimension
	 */
	public void setIcyDimension(IcyDimension src) {
		setSelectedItem(src);
	}
	
	/**
	 * Feed the model
	 */
	private void feedModel() {
		_model.addElement(IcyDimension.X);
		_model.addElement(IcyDimension.Y);
		_model.addElement(IcyDimension.Z);
		_model.addElement(IcyDimension.T);
		_model.addElement(IcyDimension.C);
	}
}
