package plugins.ylemontag.matlabio.gui;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;

import plugins.ylemontag.matlabio.ComplexMode;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Component letting the user choose one mode of import for complex objects
 */
public class ComplexModeComponent extends JComboBox {

	private static final long serialVersionUID = 1L;

	private DefaultComboBoxModel _model;
	
	/**
	 * Constructor
	 */
	public ComplexModeComponent() {
		super();
		_model = new DefaultComboBoxModel();
		setModel(_model);
		feedModel();
	}
	
	/**
	 * Selected mode
	 */
	public ComplexMode getComplexMode() {
		return (ComplexMode)getSelectedItem();
	}
	
	/**
	 * Change the selected mode
	 */
	public void setComplexMode(ComplexMode src) {
		setSelectedItem(src);
	}
	
	/**
	 * Feed the model
	 */
	private void feedModel() {
		for(ComplexMode m : ComplexMode.values()) {
			_model.addElement(m);
		}
	}
}
