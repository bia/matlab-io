package plugins.ylemontag.matlabio.gui;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import plugins.ylemontag.matlabio.DimensionMapping;
import plugins.ylemontag.matlabio.IcyDimension;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Component used to configure a dimension mapping strategy
 */
public class DimensionMappingComponent extends JButton
{	
	private static final long serialVersionUID = 1L;

	/**
	 * Interface to implement in order to listen to dimension mapping changes 
	 */
	public interface DimensionMappingChangedListener
	{	
		/**
		 * Action fired when a file change occur
		 */
		public void actionPerformed();
	}
	
	private DimensionMapping _mapping;
	private LinkedList<DimensionMappingChangedListener> _listeners;
	private boolean _shuntListeners;
	
	/**
	 * Constructor
	 */
	public DimensionMappingComponent()
	{
		super();
		_mapping = new DimensionMapping();
		refreshButtonLabel();
		_listeners = new LinkedList<DimensionMappingChangedListener>();
		_shuntListeners = false;
		
		// Listener
		addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) {
				onButtonClicked();
			}
		});
	}
	
	/**
	 * Selected dimension mapping
	 */
	public DimensionMapping getDimensionMapping()
	{
		return _mapping.clone();
	}
	
	/**
	 * Dimension corresponding to X
	 */
	public int getDimensionX()
	{
		return _mapping.getDimensionX();
	}
	
	/**
	 * Dimension corresponding to Y
	 */
	public int getDimensionY()
	{
		return _mapping.getDimensionY();
	}
	
	/**
	 * Dimension corresponding to Z
	 */
	public int getDimensionZ()
	{
		return _mapping.getDimensionZ();
	}
	
	/**
	 * Dimension corresponding to T
	 */
	public int getDimensionT()
	{
		return _mapping.getDimensionT();
	}
	
	/**
	 * Dimension corresponding to C
	 */
	public int getDimensionC()
	{
		return _mapping.getDimensionC();
	}
	
	/**
	 * Set the dimension mapping
	 */
	public void setDimensions(int dimX, int dimY, int dimZ, int dimT, int dimC)
	{
		if(
				dimX==_mapping.getDimensionX() &&
				dimY==_mapping.getDimensionY() &&
				dimZ==_mapping.getDimensionZ() &&
				dimT==_mapping.getDimensionT() &&
				dimC==_mapping.getDimensionC()
		) {
			return;
		}
		_mapping.setDimensionX(dimX);
		_mapping.setDimensionY(dimY);
		_mapping.setDimensionZ(dimZ);
		_mapping.setDimensionT(dimT);
		_mapping.setDimensionC(dimC);
		refreshButtonLabel();
		fireDimensionMappingChangedListeners();
	}
	
	/**
	 * Listen to the file change events
	 */
	public void addDimensionMappingChangedListener(DimensionMappingChangedListener l)
	{
		_listeners.add(l);
	}
	
	/**
	 * Return true if the listeners are disabled
	 */
	public boolean getShuntListeners()
	{
		return _shuntListeners;
	}
	
	/**
	 * Deactivate or re-activate the listeners
	 */
	public void setShuntListeners(boolean value)
	{
		_shuntListeners = value;
	}
	
	/**
	 * Fire the listeners
	 */
	private void fireDimensionMappingChangedListeners()
	{
		if(_shuntListeners) {
			return;
		}
		for(DimensionMappingChangedListener l : _listeners) {
			l.actionPerformed();
		}
	}
	
	/**
	 * Action performed when the user click on the button
	 */
	private void onButtonClicked() 
	{
		DimensionMapping currentMapping = _mapping;
		boolean canceled = false;
		while(true) {
			EditComponent chooser = new EditComponent();
			chooser.initMapping(currentMapping);
			int retVal = JOptionPane.showOptionDialog(this, chooser, "Dimension mapping",
				JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);
			if(retVal==JOptionPane.OK_OPTION) {
				currentMapping = chooser.retrieveMapping();
				if(currentMapping.isValidMapping()) {
					break;
				}
				else {
					JOptionPane.showMessageDialog(this, "Invalid dimension mapping", "Error",
						JOptionPane.ERROR_MESSAGE);
				}
			}
			else {
				canceled = true;
				break;
			}
		}
		if(!canceled) {
			setDimensions(
				currentMapping.getDimensionX(),
				currentMapping.getDimensionY(),
				currentMapping.getDimensionZ(),
				currentMapping.getDimensionT(),
				currentMapping.getDimensionC()
			);
		}
	}
	
	/**
	 * Refresh the mapping description on the button label
	 */
	private void refreshButtonLabel()
	{
		setText(_mapping.toString());
	}
	
	/**
	 * Component used in the edit dialog
	 */
	private static class EditComponent extends JPanel
	{
		private static final long serialVersionUID = 1L;
		
		private IcyDimensionComponent[] _mapping;
		
		/**
		 * Constructor
		 */
		public EditComponent()
		{
			setLayout(new GridLayout(5, 2, 5, 5));
			_mapping = new IcyDimensionComponent[5];
			for(int k=0; k<5; ++k) {
				_mapping[k] = new IcyDimensionComponent();
				add(new JLabel("Dimension " + (k+1)));
				add(_mapping[k]);
			}
		}
		
		/**
		 * Initialize the component with an existing DimensionMapping object
		 */
		public void initMapping(DimensionMapping src)
		{
			for(IcyDimension dim : IcyDimension.values()) {
				int v = src.getDimension(dim);
				if(v>=0 && v<_mapping.length) {
					_mapping[v].setIcyDimension(dim);
				}
			}
		}
		
		/**
		 * Retrieve the customized DimensionMapping object
		 */
		public DimensionMapping retrieveMapping()
		{
			DimensionMapping retVal = new DimensionMapping();
			for(int k=0; k<_mapping.length; ++k) {
				IcyDimension dim = _mapping[k].getIcyDimension();
				retVal.setDimension(dim, k);
			}
			return retVal;
		}
	}
}
