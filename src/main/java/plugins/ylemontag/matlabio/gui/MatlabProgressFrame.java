package plugins.ylemontag.matlabio.gui;

import icy.gui.frame.progress.CancelableProgressFrame;

import java.awt.event.ActionEvent;
import java.util.Timer;
import java.util.TimerTask;

import plugins.ylemontag.matlabio.lib.Controller;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Progress frame for the Matlab I/O operations
 */
public class MatlabProgressFrame extends CancelableProgressFrame
{
	private Controller _controller;
	private Timer _timer;
	
	/**
	 * Constructor
	 */
	public MatlabProgressFrame(String message, Controller controller)
	{
		super(message);
		_controller = controller;
		setLength(1.0);
		_timer = new Timer();
		_timer.schedule(new TimerTask()
		{	
			@Override
			public void run() {
				setPosition(_controller.getCurrentProgress());
			}
		}, 0, 100);
	}
	
	@Override
	public void actionPerformed(ActionEvent e)
	{
		_controller.cancelComputation();
		setMessage("Submitting cancel request...");
	}
}
