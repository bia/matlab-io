package plugins.ylemontag.matlabio.gui;

import icy.gui.main.MainAdapter;
import icy.gui.main.MainEvent;
import icy.main.Icy;
import icy.sequence.Sequence;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;

import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

/**
 * 
 * @author Yoann Le Montagner
 *
 * Select an opened sequence by clicking on a button
 */
public class SequenceButton extends JButton
{
	private static final long serialVersionUID = 1L;
	
	/**
	 * Listener to implement to watch at events forwarded by this component
	 */
	public interface SequenceButtonListener
	{	
		/**
		 * Triggered method
		 * @param seq Selected sequence
		 */
		public void eventTriggered(Sequence seq);
	}
	
	/**
	 * Enabling/disabling mode for the component
	 */
	public enum Mode
	{
		ALWAYS_ENABLED ,
		AUTOMATIC      ,
		ALWAYS_DISABLED
	}

	private JPopupMenu _popupMenu;
	private LinkedList<SequenceButtonListener> _listeners;
	private Mode _mode;
	
	/**
	 * Constructor
	 */
	public SequenceButton(String label) 
	{
		super(label);
		_mode      = Mode.AUTOMATIC;
		_listeners = new LinkedList<SequenceButtonListener>();
		_popupMenu = new JPopupMenu();
		addActionListener(new ActionListener()
		{	
			@Override
			public void actionPerformed(ActionEvent e) {
				showPopup();
			}
		});
		Icy.getMainInterface().addListener(new MainAdapter()
		{	
			@Override
			public void sequenceOpened(MainEvent event) {
				refreshSensitivity();
			}
			
			@Override
			public void sequenceClosed(MainEvent event) {
				refreshSensitivity();
			}
		});
		refreshSensitivity();
	}
	
	/**
	 * Current enabling/disabling mode
	 */
	public Mode getMode()
	{
		return _mode;
	}
	
	/**
	 * Change the current enabling/disabling mode
	 */
	public void setMode(Mode newMode)
	{
		_mode = newMode;
		refreshSensitivity();
	}
	
	/**
	 * Add a new listener
	 */
	public void addSequenceButtonListener(SequenceButtonListener l)
	{
		_listeners.add(l);
	}
	
	/**
	 * Display the popup menu
	 */
	private void showPopup()
	{
		_popupMenu.removeAll();
		for(final Sequence seq : Icy.getMainInterface().getSequences()) {
			JMenuItem menuItem = new JMenuItem(seq.getName());
			menuItem.addActionListener(new ActionListener()
			{	
				@Override
				public void actionPerformed(ActionEvent e) {
					fireClickOnMenuItem(seq);
				}
			});
			_popupMenu.add(menuItem);
		}
		_popupMenu.show(this, 0, getHeight());
	}
	
	/**
	 * Refresh the enable state of the button
	 */
	private void refreshSensitivity()
	{
		boolean enable = false;
		switch(_mode) {
			case ALWAYS_ENABLED : enable = true; break;
			case AUTOMATIC      : enable = Icy.getMainInterface().getSequences().size()>0; break;
			case ALWAYS_DISABLED: enable = false; break;
		}
		setEnabled(enable);
	}
	
	/**
	 * Respond to a click on a menu item
	 */
	private void fireClickOnMenuItem(Sequence seq)
	{
		for(SequenceButtonListener l : _listeners) {
			l.eventTriggered(seq);
		}
	}
}
