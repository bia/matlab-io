package plugins.ylemontag.matlabio;

import icy.sequence.Sequence;

import java.io.IOException;

import plugins.ylemontag.matlabio.Cursor.ModeC;
import plugins.ylemontag.matlabio.lib.MLIOException;
import plugins.ylemontag.matlabio.lib.MLIStream;
import plugins.ylemontag.matlabio.lib.MLIStreams;
import plugins.ylemontag.matlabio.lib.MLOStream;
import plugins.ylemontag.matlabio.lib.MLOStreams;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Specialized cursors supporting different types of sequences
 */
public class Cursors
{
	/**
	 * Factory function for sequence-writing cursors
	 */
	static public Cursor create(Sequence seq, DimensionMapping mapping, ModeC modeC, MLIStream data)
		throws MLIOException
	{
		switch(data.getType()) {
			case LOGICAL: return new LogicalCursorW      (seq, mapping, modeC, (MLIStreams.Logical)data);
			case DOUBLE : return new DoubleCursorW       (seq, mapping, modeC, (MLIStreams.Double )data);
			case SINGLE : return new FloatCursorW        (seq, mapping, modeC, (MLIStreams.Single )data);
			case INT8   : return new SignedByteCursorW   (seq, mapping, modeC, (MLIStreams.Int8   )data);
			case UINT8  : return new UnsignedByteCursorW (seq, mapping, modeC, (MLIStreams.UInt8  )data);
			case INT16  : return new SignedShortCursorW  (seq, mapping, modeC, (MLIStreams.Int16  )data);
			case UINT16 : return new UnsignedShortCursorW(seq, mapping, modeC, (MLIStreams.UInt16 )data);
			case INT32  : return new SignedIntCursorW    (seq, mapping, modeC, (MLIStreams.Int32  )data);
			case UINT32 : return new UnsignedIntCursorW  (seq, mapping, modeC, (MLIStreams.UInt32 )data);
			default:
				throw new MLIOException("Unsupported Matlab type: " + data.getType());
		}
	}
	
	/**
	 * Factory function for sequence-reading cursors
	 */
	static public Cursor create(Sequence seq, DimensionMapping mapping, ModeC modeC, MLOStream data)
		throws MLIOException
	{
		switch(data.getType()) {
			case DOUBLE: return new DoubleCursorR       (seq, mapping, modeC, (MLOStreams.Double)data);
			case SINGLE: return new FloatCursorR        (seq, mapping, modeC, (MLOStreams.Single)data);
			case INT8  : return new SignedByteCursorR   (seq, mapping, modeC, (MLOStreams.Int8  )data);
			case UINT8 : return new UnsignedByteCursorR (seq, mapping, modeC, (MLOStreams.UInt8 )data);
			case INT16 : return new SignedShortCursorR  (seq, mapping, modeC, (MLOStreams.Int16 )data);
			case UINT16: return new UnsignedShortCursorR(seq, mapping, modeC, (MLOStreams.UInt16)data);
			case INT32 : return new SignedIntCursorR    (seq, mapping, modeC, (MLOStreams.Int32 )data);
			case UINT32: return new UnsignedIntCursorR  (seq, mapping, modeC, (MLOStreams.UInt32)data);
			default:
				throw new MLIOException("Unsupported Matlab type: " + data.getType());
		}
	}
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Base-class cursors
	
	/**
	 * Cursor for double-typed sequences
	 */
	private static abstract class DoubleCursor extends Cursor
	{
		private double[][][][] _pointerTZCXY;
		private double[][][] _pointerZCXY;
		private double[][] _pointerCXY;
		protected double[] _pointerXY;
		
		public DoubleCursor(Sequence seq, DimensionMapping mapping, ModeC modeC)
			throws DimensionMapping.BadMapping
		{
			super(seq, mapping, modeC);
			_pointerTZCXY = _seq.getDataXYCZTAsDouble();
			notifyPosTChanged();
		}

		@Override
		protected void notifyPosZChanged()
		{
			_pointerCXY = _pointerZCXY[_posZ];
			notifyPosCChanged();
		}

		@Override
		protected void notifyPosTChanged()
		{
			_pointerZCXY = _pointerTZCXY[_posT];
			notifyPosZChanged();
		}

		@Override
		protected void notifyPosCChanged()
		{
			_pointerXY = _pointerCXY[_posC];
		}
	}
	
	/**
	 * Cursor for float-typed sequences
	 */
	private static abstract class FloatCursor extends Cursor
	{
		private float[][][][] _pointerTZCXY;
		private float[][][] _pointerZCXY;
		private float[][] _pointerCXY;
		protected float[] _pointerXY;
		
		public FloatCursor(Sequence seq, DimensionMapping mapping, ModeC modeC)
			throws DimensionMapping.BadMapping
		{
			super(seq, mapping, modeC);
			_pointerTZCXY = _seq.getDataXYCZTAsFloat();
			notifyPosTChanged();
		}

		@Override
		protected void notifyPosZChanged()
		{
			_pointerCXY = _pointerZCXY[_posZ];
			notifyPosCChanged();
		}

		@Override
		protected void notifyPosTChanged()
		{
			_pointerZCXY = _pointerTZCXY[_posT];
			notifyPosZChanged();
		}

		@Override
		protected void notifyPosCChanged()
		{
			_pointerXY = _pointerCXY[_posC];
		}
	}
	
	/**
	 * Cursor for byte-typed sequences
	 */
	private static abstract class ByteCursor extends Cursor
	{
		private byte[][][][] _pointerTZCXY;
		private byte[][][] _pointerZCXY;
		private byte[][] _pointerCXY;
		protected byte[] _pointerXY;
		
		public ByteCursor(Sequence seq, DimensionMapping mapping, ModeC modeC)
			throws DimensionMapping.BadMapping
		{
			super(seq, mapping, modeC);
			_pointerTZCXY = _seq.getDataXYCZTAsByte();
			notifyPosTChanged();
		}

		@Override
		protected void notifyPosZChanged()
		{
			_pointerCXY = _pointerZCXY[_posZ];
			notifyPosCChanged();
		}

		@Override
		protected void notifyPosTChanged()
		{
			_pointerZCXY = _pointerTZCXY[_posT];
			notifyPosZChanged();
		}

		@Override
		protected void notifyPosCChanged()
		{
			_pointerXY = _pointerCXY[_posC];
		}
	}
	
	/**
	 * Cursor for short-typed sequences
	 */
	private static abstract class ShortCursor extends Cursor
	{
		private short[][][][] _pointerTZCXY;
		private short[][][] _pointerZCXY;
		private short[][] _pointerCXY;
		protected short[] _pointerXY;
		
		public ShortCursor(Sequence seq, DimensionMapping mapping, ModeC modeC)
			throws DimensionMapping.BadMapping
		{
			super(seq, mapping, modeC);
			_pointerTZCXY = _seq.getDataXYCZTAsShort();
			notifyPosTChanged();
		}

		@Override
		protected void notifyPosZChanged()
		{
			_pointerCXY = _pointerZCXY[_posZ];
			notifyPosCChanged();
		}

		@Override
		protected void notifyPosTChanged()
		{
			_pointerZCXY = _pointerTZCXY[_posT];
			notifyPosZChanged();
		}

		@Override
		protected void notifyPosCChanged()
		{
			_pointerXY = _pointerCXY[_posC];
		}
	}
	
	/**
	 * Cursor for int-typed sequences
	 */
	private static abstract class IntCursor extends Cursor
	{
		private int[][][][] _pointerTZCXY;
		private int[][][] _pointerZCXY;
		private int[][] _pointerCXY;
		protected int[] _pointerXY;
		
		public IntCursor(Sequence seq, DimensionMapping mapping, ModeC modeC)
			throws DimensionMapping.BadMapping
		{
			super(seq, mapping, modeC);
			_pointerTZCXY = _seq.getDataXYCZTAsInt();
			notifyPosTChanged();
		}

		@Override
		protected void notifyPosZChanged()
		{
			_pointerCXY = _pointerZCXY[_posZ];
			notifyPosCChanged();
		}

		@Override
		protected void notifyPosTChanged()
		{
			_pointerZCXY = _pointerTZCXY[_posT];
			notifyPosZChanged();
		}

		@Override
		protected void notifyPosCChanged()
		{
			_pointerXY = _pointerCXY[_posC];
		}
	}
	
	
	
	//////////////////////////////////////////////////////////////////////////////
	// Sequence-writing cursors
	
	/**
	 * Write in a double-valued sequence
	 */
	private static class DoubleCursorW extends DoubleCursor
	{
		private MLIStreams.Double _stream;
		
		public DoubleCursorW(Sequence seq, DimensionMapping mapping, ModeC modeC, MLIStreams.Double data)
			throws DimensionMapping.BadMapping
		{
			super(seq, mapping, modeC);
			_stream = data;
		}

		@Override
		public void consume() throws IOException
		{
			_pointerXY[_posXY] = _stream.get();
		}
	}
	
	/**
	 * Write in a float-valued sequence
	 */
	private static class FloatCursorW extends FloatCursor
	{
		private MLIStreams.Single _stream;
		
		public FloatCursorW(Sequence seq, DimensionMapping mapping, ModeC modeC, MLIStreams.Single data)
			throws DimensionMapping.BadMapping
		{
			super(seq, mapping, modeC);
			_stream = data;
		}

		@Override
		public void consume() throws IOException
		{
			_pointerXY[_posXY] = _stream.get();
		}
	}
	
	/**
	 * Write in a signed byte-valued sequence
	 */
	private static class SignedByteCursorW extends ByteCursor
	{
		private MLIStreams.Int8 _stream;
		
		public SignedByteCursorW(Sequence seq, DimensionMapping mapping, ModeC modeC, MLIStreams.Int8 data)
			throws DimensionMapping.BadMapping
		{
			super(seq, mapping, modeC);
			_stream = data;
		}

		@Override
		public void consume() throws IOException
		{
			_pointerXY[_posXY] = _stream.get();	
		}
	}
	
	/**
	 * Write in an unsigned byte-valued sequence
	 */
	private static class UnsignedByteCursorW extends ByteCursor
	{
		private MLIStreams.UInt8 _stream;
		
		public UnsignedByteCursorW(Sequence seq, DimensionMapping mapping, ModeC modeC, MLIStreams.UInt8 data)
			throws DimensionMapping.BadMapping
		{
			super(seq, mapping, modeC);
			_stream = data;
		}

		@Override
		public void consume() throws IOException
		{
			_pointerXY[_posXY] = _stream.get();	
		}
	}
	
	/**
	 * Write in an unsigned byte-valued sequence from boolean-valued data
	 */
	private static class LogicalCursorW extends ByteCursor
	{
		private MLIStreams.Logical _stream;
		
		public LogicalCursorW(Sequence seq, DimensionMapping mapping, ModeC modeC, MLIStreams.Logical data)
			throws DimensionMapping.BadMapping
		{
			super(seq, mapping, modeC);
			_stream = data;
		}

		@Override
		public void consume() throws IOException
		{
			_pointerXY[_posXY] = _stream.get() ? (byte)1 : (byte)0;	
		}
	}
	
	/**
	 * Write in a signed short-valued sequence
	 */
	private static class SignedShortCursorW extends ShortCursor
	{
		private MLIStreams.Int16 _stream;
		
		public SignedShortCursorW(Sequence seq, DimensionMapping mapping, ModeC modeC, MLIStreams.Int16 data)
			throws DimensionMapping.BadMapping
		{
			super(seq, mapping, modeC);
			_stream = data;
		}

		@Override
		public void consume() throws IOException
		{
			_pointerXY[_posXY] = _stream.get();	
		}
	}
	
	/**
	 * Write in an unsigned short-valued sequence
	 */
	private static class UnsignedShortCursorW extends ShortCursor
	{
		private MLIStreams.UInt16 _stream;
		
		public UnsignedShortCursorW(Sequence seq, DimensionMapping mapping, ModeC modeC, MLIStreams.UInt16 data)
			throws DimensionMapping.BadMapping
		{
			super(seq, mapping, modeC);
			_stream = data;
		}

		@Override
		public void consume() throws IOException
		{
			_pointerXY[_posXY] = _stream.get();	
		}
	}
	
	/**
	 * Write in a signed int-valued sequence
	 */
	private static class SignedIntCursorW extends IntCursor
	{
		private MLIStreams.Int32 _stream;
		
		public SignedIntCursorW(Sequence seq, DimensionMapping mapping, ModeC modeC, MLIStreams.Int32 data)
			throws DimensionMapping.BadMapping
		{
			super(seq, mapping, modeC);
			_stream = data;
		}

		@Override
		public void consume() throws IOException
		{
			_pointerXY[_posXY] = _stream.get();	
		}
	}
	
	/**
	 * Write in an unsigned int-valued sequence
	 */
	private static class UnsignedIntCursorW extends IntCursor
	{
		private MLIStreams.UInt32 _stream;
		
		public UnsignedIntCursorW(Sequence seq, DimensionMapping mapping, ModeC modeC, MLIStreams.UInt32 data)
			throws DimensionMapping.BadMapping
		{
			super(seq, mapping, modeC);
			_stream = data;
		}

		@Override
		public void consume() throws IOException
		{
			_pointerXY[_posXY] = _stream.get();	
		}
	}
	
	
	
	//////////////////////////////////////////////////////////////////////////////
	// Sequence-reading cursors
	
	/**
	 * Write in a double-valued sequence
	 */
	private static class DoubleCursorR extends DoubleCursor
	{
		private MLOStreams.Double _stream;
		
		public DoubleCursorR(Sequence seq, DimensionMapping mapping, ModeC modeC, MLOStreams.Double data)
			throws DimensionMapping.BadMapping
		{
			super(seq, mapping, modeC);
			_stream = data;
		}

		@Override
		public void consume() throws IOException
		{
			_stream.put(_pointerXY[_posXY]);
		}
	}
	
	/**
	 * Read in a float-valued sequence
	 */
	private static class FloatCursorR extends FloatCursor
	{
		private MLOStreams.Single _stream;
		
		public FloatCursorR(Sequence seq, DimensionMapping mapping, ModeC modeC, MLOStreams.Single data)
			throws DimensionMapping.BadMapping
		{
			super(seq, mapping, modeC);
			_stream = data;
		}

		@Override
		public void consume() throws IOException
		{
			_stream.put(_pointerXY[_posXY]);
		}
	}
	
	/**
	 * Read in a signed byte-valued sequence
	 */
	private static class SignedByteCursorR extends ByteCursor
	{
		private MLOStreams.Int8 _stream;
		
		public SignedByteCursorR(Sequence seq, DimensionMapping mapping, ModeC modeC, MLOStreams.Int8 data)
			throws DimensionMapping.BadMapping
		{
			super(seq, mapping, modeC);
			_stream = data;
		}

		@Override
		public void consume() throws IOException
		{
			_stream.put(_pointerXY[_posXY]);	
		}
	}
	
	/**
	 * Read in an unsigned byte-valued sequence
	 */
	private static class UnsignedByteCursorR extends ByteCursor
	{
		private MLOStreams.UInt8 _stream;
		
		public UnsignedByteCursorR(Sequence seq, DimensionMapping mapping, ModeC modeC, MLOStreams.UInt8 data)
			throws DimensionMapping.BadMapping
		{
			super(seq, mapping, modeC);
			_stream = data;
		}

		@Override
		public void consume() throws IOException
		{
			_stream.put(_pointerXY[_posXY]);	
		}
	}
	
	/**
	 * Read in a signed short-valued sequence
	 */
	private static class SignedShortCursorR extends ShortCursor
	{
		private MLOStreams.Int16 _stream;
		
		public SignedShortCursorR(Sequence seq, DimensionMapping mapping, ModeC modeC, MLOStreams.Int16 data)
			throws DimensionMapping.BadMapping
		{
			super(seq, mapping, modeC);
			_stream = data;
		}

		@Override
		public void consume() throws IOException
		{
			_stream.put(_pointerXY[_posXY]);	
		}
	}
	
	/**
	 * Read in an unsigned short-valued sequence
	 */
	private static class UnsignedShortCursorR extends ShortCursor
	{
		private MLOStreams.UInt16 _stream;
		
		public UnsignedShortCursorR(Sequence seq, DimensionMapping mapping, ModeC modeC, MLOStreams.UInt16 data)
			throws DimensionMapping.BadMapping
		{
			super(seq, mapping, modeC);
			_stream = data;
		}

		@Override
		public void consume() throws IOException
		{
			_stream.put(_pointerXY[_posXY]);
		}
	}
	
	/**
	 * Read in a signed int-valued sequence
	 */
	private static class SignedIntCursorR extends IntCursor
	{
		private MLOStreams.Int32 _stream;
		
		public SignedIntCursorR(Sequence seq, DimensionMapping mapping, ModeC modeC, MLOStreams.Int32 data)
			throws DimensionMapping.BadMapping
		{
			super(seq, mapping, modeC);
			_stream = data;
		}

		@Override
		public void consume() throws IOException
		{
			_stream.put(_pointerXY[_posXY]);
		}
	}
	
	/**
	 * Read in an unsigned int-valued sequence
	 */
	private static class UnsignedIntCursorR extends IntCursor
	{
		private MLOStreams.UInt32 _stream;
		
		public UnsignedIntCursorR(Sequence seq, DimensionMapping mapping, ModeC modeC, MLOStreams.UInt32 data)
			throws DimensionMapping.BadMapping
		{
			super(seq, mapping, modeC);
			_stream = data;
		}

		@Override
		public void consume() throws IOException
		{
			_stream.put(_pointerXY[_posXY]);
		}
	}
}
