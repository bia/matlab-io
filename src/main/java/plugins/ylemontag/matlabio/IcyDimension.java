package plugins.ylemontag.matlabio;

/**
 * 
 * @author Yoann Le Montagner
 *
 * The 5 dimensions defined by the ICY API
 */
public enum IcyDimension
{
	X("X", 0),
	Y("Y", 1),
	Z("Z", 2),
	T("T", 3),
	C("C", 4);
	
	private String _text;
	private int _code;
	
	/**
	 * Constructor
	 */
	private IcyDimension(String text, int code)
	{
		_text = text;
		_code = code;
	}
	
	/**
	 * Integer code
	 */
	public int getCode()
	{
		return _code;
	}
	
	@Override
	public String toString()
	{
		return _text;
	}
}
