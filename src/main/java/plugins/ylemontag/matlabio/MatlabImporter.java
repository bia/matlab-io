package plugins.ylemontag.matlabio;

import icy.image.IcyBufferedImage;
import icy.sequence.Sequence;
import icy.type.DataType;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import plugins.ylemontag.matlabio.lib.Controller;
import plugins.ylemontag.matlabio.lib.MLIOException;
import plugins.ylemontag.matlabio.lib.MLIStream;
import plugins.ylemontag.matlabio.lib.MLMeta;
import plugins.ylemontag.matlabio.lib.MLObject;
import plugins.ylemontag.matlabio.lib.MatFileReader;

/**
 * 
 * @author Yoann Le Montagner
 *
 * This class is used to browse a Matlab .mat and to import some part of
 * its content as a sequence.
 */
public class MatlabImporter
{
	private MatFileReader _reader;
	
	/**
	 * Constructor
	 * @param path Path of the .mat file
	 */
	public MatlabImporter(String path) throws IOException
	{
		this(new MatFileReader(path));
	}
	
	/**
	 * Constructor
	 * @param file Object pointing to a .mat file
	 */
	public MatlabImporter(File file) throws IOException
	{
		this(new MatFileReader(file));
	}
	
	/**
	 * Constructor
	 * @param reader Low-level .mat file reader
	 */
	public MatlabImporter(MatFileReader reader) throws MLIOException
	{
		if(reader==null) {
			throw new MLIOException(
				"Trying to construct a MatlabImporter object from a null MatFileReader object"
			);
		}
		_reader = reader;
	}
	
	/**
	 * Return the list of variable names stored in the .mat file
	 */
	public Set<String> getVariables()
	{
		return _reader.getKeys();
	}
	
	/**
	 * Return the list of importable sequences stored in the .mat file
	 * A variable can be imported if it is of type MLNumericArray and if it
	 * has at most 5 dimensions.
	 */
	public Set<String> getImportableSequences()
	{
		Set<String> retVal = new HashSet<String>();
		Set<String> keys = getVariables();
		for(String key : keys) {
			MLMeta meta = _reader.getMeta(key);
			if(isImportableAsSequence(meta)) {
				retVal.add(key);
			}
		}
		return retVal;
	}
	
	/**
	 * Return the meta informations associated to a given Matlab variable
	 * @param name Name of the variable in the Matlab .mat file
	 */
	public MLMeta getMeta(String name)
	{
		return _reader.getMeta(name);
	}
	
	/**
	 * Check whether a Matlab variable can be imported as a sequence or not
	 */
	public boolean isImportableAsSequence(String name)
	{
		MLMeta meta = getMeta(name);
		return meta==null ? false : isImportableAsSequence(meta);
	}
	
	/**
	 * Check whether a Matlab variable can be imported as a sequence or not
	 */
	public static boolean isImportableAsSequence(MLMeta meta)
	{
		if(meta.getDimensions().length>5) {
			return false;
		}
		switch(meta.getType()) {
			case LOGICAL:
			case DOUBLE :
			case SINGLE :
			case INT8   :
			case UINT8  :
			case INT16  :
			case UINT16 :
			case INT32  :
			case UINT32 :
				return true;
			default:
				return false;
		}
	}
	
	/**
	 * Import a variable as a new sequence
	 * @param name Name of the variable in the Matlab .mat file
	 * @param mapping Object used to describe how to interpret the dimensions of a Matlab object
	 * @param complexMode Define how complex-valued Matlab arrays should be imported 
	 */
	public Sequence getSequence(String name, DimensionMapping mapping, ComplexMode complexMode)
		throws IOException
	{
		try {
			return getSequence(name, mapping, complexMode, new Controller());
		}
		catch(Controller.CanceledByUser err) {
			throw new RuntimeException("Unreachable code point");
		}
	}
	
	/**
	 * Import a variable as a new sequence
	 * @param name Name of the variable in the Matlab .mat file
	 * @param mapping Object used to describe how to interpret the dimensions of a Matlab object
	 * @param complexMode Define how complex-valued Matlab arrays should be imported 
	 * @param outName The name that will be affected to the imported sequence
	 */
	public Sequence getSequence(String name, DimensionMapping mapping, ComplexMode complexMode,
		String outName) throws IOException
	{
		try {
			return getSequence(name, mapping, complexMode, outName, new Controller());
		}
		catch(Controller.CanceledByUser err) {
			throw new RuntimeException("Unreachable code point");
		}
	}
	
	/**
	 * Import a variable as a new sequence
	 * @param name Name of the variable in the Matlab .mat file
	 * @param mapping Object used to describe how to interpret the dimensions of a Matlab object
	 * @param complexMode Define how complex-valued Matlab arrays should be imported 
	 * @param out Output sequence
	 */
	public void getSequence(String name, DimensionMapping mapping, ComplexMode complexMode,
		Sequence out) throws IOException
	{
		try {
			getSequence(name, mapping, complexMode, out, new Controller());
		}
		catch(Controller.CanceledByUser err) {
			throw new RuntimeException("Unreachable code point");
		}
	}
	
	/**
	 * Import a variable as a new sequence
	 * @param name Name of the variable in the Matlab .mat file
	 * @param mapping Object used to describe how to interpret the dimensions of a Matlab object
	 * @param complexMode Define how complex-valued Matlab arrays should be imported 
	 * @param controller Manager for the working thread 
	 */
	public Sequence getSequence(String name, DimensionMapping mapping, ComplexMode complexMode,
		Controller controller) throws IOException, Controller.CanceledByUser
	{
		return getSequence(name, mapping, complexMode, name, controller);
	}
	
	/**
	 * Import a variable as a new sequence
	 * @param name Name of the variable in the Matlab .mat file
	 * @param mapping Object used to describe how to interpret the dimensions of a Matlab object
	 * @param complexMode Define how complex-valued Matlab arrays should be imported 
	 * @param outName The name that will be affected to the imported sequence
	 * @param controller Manager for the working thread 
	 */
	public Sequence getSequence(String name, DimensionMapping mapping, ComplexMode complexMode,
		String outName, Controller controller) throws IOException, Controller.CanceledByUser
	{
		Sequence retVal = new Sequence();
		retVal.setName(outName);
		getSequence(name, mapping, complexMode, retVal, controller);
		return retVal;
	}
	
	/**
	 * Import a variable as a new sequence
	 * @param name Name of the variable in the Matlab .mat file
	 * @param mapping Object used to describe how to interpret the dimensions of a Matlab object
	 * @param complexMode Define how complex-valued Matlab arrays should be imported 
	 * @param out Output sequence
	 * @param controller Manager for the working thread 
	 */
	public void getSequence(String name, DimensionMapping mapping, ComplexMode complexMode,
		Sequence out, Controller controller) throws IOException, Controller.CanceledByUser
	{
		// Raw data
		controller.checkPoint();
		MLIStream rawData = _reader.getDataAsStream(name);
		controller.checkPoint();
		
		// Size and datatype of the sequence
		boolean isComplex = rawData.getIsComplex();
		int sizeX = computeSize(mapping, IcyDimension.X, rawData);
		int sizeY = computeSize(mapping, IcyDimension.Y, rawData);
		int sizeZ = computeSize(mapping, IcyDimension.Z, rawData);
		int sizeT = computeSize(mapping, IcyDimension.T, rawData);
		int sizeC = computeSize(mapping, IcyDimension.C, rawData);
		int numElems = sizeX*sizeY*sizeZ*sizeT*sizeC;
		if(complexMode==ComplexMode.BOTH && isComplex) {
			sizeC *= 2;
		}
		DataType outType = computeType(rawData);
		
		// If necessary, allocate the output sequence
		controller.checkPoint();
		if(outType!=out.getDataType_()
			|| out.getSizeX()!=sizeX
			|| out.getSizeY()!=sizeY
			|| out.getSizeZ()!=sizeZ
			|| out.getSizeT()!=sizeT
			|| out.getSizeC()!=sizeC
		) {
			out.beginUpdate();
			try {
				out.removeAllImages();
				for(int t=0; t<sizeT; ++t) {
					for(int z=0; z<sizeZ; ++z) {
						IcyBufferedImage currentFrame = new IcyBufferedImage(sizeX, sizeY, sizeC, outType);
						out.setImage(t, z, currentFrame);
						controller.checkPoint();
					}
				}
			}
			finally {
				out.endUpdate();
			}
		}
		
		// Feed the sequence
		controller.startCounter(isComplex && complexMode!=ComplexMode.REAL_PART ? 2*numElems : numElems);
		if(isComplex) {
			switch(complexMode)
			{
				case REAL_PART:
					feedSequence(out, mapping, Cursor.ModeC.ALL_C, rawData, controller);
					break;
				
				case IMAGINARY_PART:
					rawData.skip(numElems, controller);
					feedSequence(out, mapping, Cursor.ModeC.ALL_C, rawData, controller);
					break;
				
				case BOTH:
					feedSequence(out, mapping, Cursor.ModeC.EVEN_C, rawData, controller);
					feedSequence(out, mapping, Cursor.ModeC.ODD_C , rawData, controller);
					break;
			}
		}
		else {
			feedSequence(out, mapping, Cursor.ModeC.ALL_C, rawData, controller);
		}
		controller.stopCounter();
		out.dataChanged();
	}
	
	/**
	 * Feed a sequence (one-pass)
	 */
	private void feedSequence(Sequence seq, DimensionMapping mapping, Cursor.ModeC modeC, MLIStream rawData,
		Controller controller) throws IOException, Controller.CanceledByUser
	{
		for(Cursor curs=Cursors.create(seq, mapping, modeC, rawData); curs.isValidPosition(); curs.next()) {
			controller.checkPointNext();
			curs.consume();
		}
	}
	
	/**
	 * Compute the dimension of the sequence to be created according to the
	 * current dimension mapping and the dimensions of the Matlab object
	 */
	private int computeSize(DimensionMapping mapping, IcyDimension icyDim, MLObject rawData)
	{
		int rawDim = mapping.getDimension(icyDim);
		int[] rawSize = rawData.getDimensions();
		return rawDim<rawSize.length ? rawSize[rawDim] : 1;
	}
	
	/**
	 * Return the correct sequence datatype according to the type of the data
	 * embedded in the Matlab object
	 */
	static private DataType computeType(MLObject rawData) throws MLIOException
	{
		switch(rawData.getType()) {
			case LOGICAL: return DataType.UBYTE ;
			case DOUBLE : return DataType.DOUBLE;
			case SINGLE : return DataType.FLOAT ;
			case INT8   : return DataType.BYTE  ;
			case UINT8  : return DataType.UBYTE ;
			case INT16  : return DataType.SHORT ;
			case UINT16 : return DataType.USHORT;
			case INT32  : return DataType.INT   ;
			case UINT32 : return DataType.UINT  ;
			default:
				throw new MLIOException("Unsupported Matlab type: " + rawData.getType());
		}
	}
}
