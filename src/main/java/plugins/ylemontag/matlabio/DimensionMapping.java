package plugins.ylemontag.matlabio;

import plugins.ylemontag.matlabio.lib.MLIOException;


/**
 * 
 * @author Yoann Le Montagner
 *
 * Define how to interpret the dimensions of a Matlab array
 */
public class DimensionMapping implements Cloneable
{
	/**
	 * Exception thrown by the getInverseMapping function
	 */
	public class BadMapping extends MLIOException
	{
		private static final long serialVersionUID = 1L;
		
		/**
		 * Constructor
		 */
		public BadMapping()
		{
			super("Invalid dimension mapping");
		}
	}
	
	// Ex: _mapping[1] = 3; to map the 4th Matlab dimension to Y
	private int[] _mapping;
	
	/**
	 * Constructor
	 * 
	 * Default behavior:
	 *   Y <-> Matlab dimension 1
	 *   X <-> Matlab dimension 2
	 *   Z <-> Matlab dimension 3
	 *   T <-> Matlab dimension 4
	 *   C <-> Matlab dimension 5
	 */
	public DimensionMapping()
	{
		_mapping = new int[5];
		setDimensionY(0);
		setDimensionX(1);
		setDimensionZ(2);
		setDimensionT(3);
		setDimensionC(4);
	}
	
	@Override
	public DimensionMapping clone()
	{
		DimensionMapping retVal = new DimensionMapping();
		for(int k=0; k<5; ++k) {
			retVal._mapping[k] = _mapping[k];
		}
		return retVal;
	}
	
	@Override
	public String toString()
	{
		try {
			IcyDimension[] inverseMap = getInverseMapping();
			String retVal = "";
			for(int k=0; k<inverseMap.length; ++k) {
				if(k!=0) {
					retVal += "\u00d7";
				}
				retVal += inverseMap[k].toString();
			}
			return retVal;
		}
		catch(BadMapping err) {
			return "Invalid dimension mapping";
		}
	}
	
	/**
	 * Check if the dimension mapping is valid, i.e. if _mapping actually
	 * represents a permutation of {0, 1, ..., 4}
	 */
	public boolean isValidMapping()
	{
		try {
			ensureValidMapping();
			return true;
		}
		catch(BadMapping err) {
			return false;
		}
	}
	
	/**
	 * Throw an exception if the given mapping is not valid
	 */
	public void ensureValidMapping() throws BadMapping
	{
		getInverseMapping();
	}
	
	/**
	 * Set the Matlab dimension corresponding to X
	 */
	public void setDimensionX(int src)
	{
		setDimension(IcyDimension.X, src);
	}
	
	/**
	 * Set the Matlab dimension corresponding to Y
	 */
	public void setDimensionY(int src)
	{
		setDimension(IcyDimension.Y, src);
	}
	
	/**
	 * Set the Matlab dimension corresponding to Z
	 */
	public void setDimensionZ(int src)
	{
		setDimension(IcyDimension.Z, src);
	}
	
	/**
	 * Set the Matlab dimension corresponding to T
	 */
	public void setDimensionT(int src)
	{
		setDimension(IcyDimension.T, src);
	}
	
	/**
	 * Set the Matlab dimension corresponding to C
	 */
	public void setDimensionC(int src)
	{
		setDimension(IcyDimension.C, src);
	}
	
	/**
	 * Set a Matlab dimension
	 */
	public void setDimension(IcyDimension dim, int src)
	{
		_mapping[dim.getCode()] = src;
	}
	
	/**
	 * Dimension corresponding to X
	 */
	public int getDimensionX()
	{
		return getDimension(IcyDimension.X);
	}
	
	/**
	 * Dimension corresponding to Y
	 */
	public int getDimensionY()
	{
		return getDimension(IcyDimension.Y);
	}
	
	/**
	 * Dimension corresponding to Z
	 */
	public int getDimensionZ()
	{
		return getDimension(IcyDimension.Z);
	}
	
	/**
	 * Dimension corresponding to T
	 */
	public int getDimensionT()
	{
		return getDimension(IcyDimension.T);
	}
	
	/**
	 * Dimension corresponding to C
	 */
	public int getDimensionC()
	{
		return getDimension(IcyDimension.C);
	}
	
	/**
	 * Value of the given dimension
	 */
	public int getDimension(IcyDimension dim)
	{
		return _mapping[dim.getCode()];
	}
	
	/**
	 * Build the inverse mapping
	 */
	public IcyDimension[] getInverseMapping() throws BadMapping
	{
		IcyDimension[] retVal = new IcyDimension[5];
		for(IcyDimension dim : IcyDimension.values()) {
			int current_dim = _mapping[dim.getCode()];
			if(current_dim<0 || current_dim>=5 || retVal[current_dim]!=null) {
				throw new BadMapping();
			}
			retVal[current_dim] = dim;
		}
		return retVal;
	}
}
