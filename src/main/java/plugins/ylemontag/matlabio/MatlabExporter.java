package plugins.ylemontag.matlabio;

import icy.sequence.Sequence;
import icy.type.DataType;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import plugins.ylemontag.matlabio.lib.Controller;
import plugins.ylemontag.matlabio.lib.MLIOException;
import plugins.ylemontag.matlabio.lib.MLMeta;
import plugins.ylemontag.matlabio.lib.MLOStream;
import plugins.ylemontag.matlabio.lib.MLType;
import plugins.ylemontag.matlabio.lib.MatFileWriter;

/**
 * 
 * @author Yoann Le Montagner
 *
 * This class is used to create a Matlab .mat and to save some data in it
 */
public class MatlabExporter
{
	private MatFileWriter _writer;
	private Lock _lock;
	private Map<String, Item> _items;
	
	/**
	 * Constructor
	 * @param path Path of the .mat file
	 */
	public MatlabExporter(String path, boolean append) throws IOException
	{
		this(new MatFileWriter(path, append));
	}
	
	/**
	 * Constructor
	 * @param file Object pointing to a .mat file
	 */
	public MatlabExporter(File file, boolean append) throws IOException
	{
		this(new MatFileWriter(file, append));
	}
	
	/**
	 * Constructor
	 * @param reader Low-level .mat file reader
	 */
	public MatlabExporter(MatFileWriter writer) throws MLIOException
	{
		if(writer==null) {
			throw new MLIOException(
				"Trying to construct a MatlabExporter object from a null MatFileWriter object"
			);
		}
		_writer = writer;
		_lock = new ReentrantLock();
		_items = new HashMap<String, Item>();
	}
	
	/**
	 * Return the list of all variable (both pending and already stored)
	 */
	public Set<String> getAllVariables()
	{
		Set<String> retVal = new HashSet<String>();
		retVal.addAll(getStoredVariables ());
		retVal.addAll(getPendingVariables());
		return retVal;
	}
	
	/**
	 * Return the list of variable names already stored in the given file
	 */
	public Set<String> getStoredVariables()
	{
		return _writer.getKeys();
	}
	
	/**
	 * Return the list of variable names to be stored in the given file
	 */
	public Set<String> getPendingVariables()
	{
		return _items.keySet();
	}
	
	/**
	 * Check whether the given name corresponds to a stored variable
	 */
	public boolean isStoredVariable(String name)
	{
		return getStoredVariables().contains(name);
	}
	
	/**
	 * Check whether the given name corresponds to a pending variable
	 */
	public boolean isPendingVariable(String name)
	{
		return getPendingVariables().contains(name);
	}
	
	/**
	 * Return the meta informations associated to a given Matlab variable
	 * @param name Name of the variable in the Matlab .mat file
	 */
	public MLMeta getMeta(String name)
	{
		try {
			return retrieveItem(name).getMeta();
		}
		catch(MLIOException err) {
			return _writer.getMeta(name);
		}
	}
	
	/**
	 * Mark the given sequence to be saved in the current file
	 * @param seq Sequence to export
	 * @param name Name of the variable to give to the sequence in the Matlab .mat file
	 * @param mapping Object used to describe how to interpret the dimensions of a Matlab object
	 */
	public void putData(Sequence seq, String name, DimensionMapping mapping)
		throws MLIOException
	{
		putData(seq, name, mapping, false);
	}
	
	/**
	 * Mark the given sequence to be saved in the current file
	 * @param seq Sequence to export
	 * @param name Name of the variable to give to the sequence in the Matlab .mat file
	 * @param mapping Object used to describe how to interpret the dimensions of a Matlab object
	 * @param isComplex Whether the given sequence is complex-valued
	 */
	public void putData(final Sequence seq, final String name, final DimensionMapping mapping, final boolean isComplex)
		throws MLIOException
	{
		tryLock(new WriteOperation()
		{	
			@Override
			public void run() throws MLIOException {
				Item item = new Item(seq, name, mapping, isComplex);
				_items.put(item.getMeta().getName(), item);
			}
		});
	}
	
	/**
	 * Copy the pending data contained in 'exporter' to the current object
	 */
	public void putPendingData(final MatlabExporter exporter) throws MLIOException
	{
		tryLock(new WriteOperation()
		{	
			@Override
			public void run() throws MLIOException {
				for(Item item : exporter._items.values()) {
					String key = item._meta.getName();
					Item localItem = new Item(item._data, key, item._mapping, item._meta.getIsComplex());
					_items.put(key, localItem);
				}
			}
		});
	}
	
	/**
	 * Change the name of a variable
	 */
	public void updateName(final String oldName, final String newName) throws MLIOException
	{
		tryLock(new WriteOperation()
		{	
			@Override
			public void run() throws MLIOException {
				Item item = retrieveItem(oldName);
				item.setName(newName);
				_items.remove(oldName);
				_items.put(item.getMeta().getName(), item);
			}
		});
	}
	
	/**
	 * Change the dimension mapping affected to a variable
	 */
	public void updateDimensionMapping(final String name, final DimensionMapping mapping) throws MLIOException
	{
		tryLock(new WriteOperation()
		{	
			@Override
			public void run() throws MLIOException {
				Item item = retrieveItem(name);
				item.setDimensionMapping(mapping);
			}
		});
	}
	
	/**
	 * Change the complex flag affected to a variable
	 */
	public void updateIsComplex(final String name, final boolean isComplex) throws MLIOException
	{
		tryLock(new WriteOperation()
		{	
			@Override
			public void run() throws MLIOException {
				Item item = retrieveItem(name);
				item.setIsComplex(isComplex);
			}
		});
	}
	
	/**
	 * Remove the variable with the given name from the list of variable to export
	 */
	public void eraseData(final String name) throws MLIOException
	{
		tryLock(new WriteOperation()
		{	
			@Override
			public void run() throws MLIOException {
				_items.remove(name);
			}
		});
	}
	
	
	/**
	 * Clear the list of variable to export
	 */
	public void eraseAll() throws MLIOException
	{
		tryLock(new WriteOperation()
		{
			@Override
			public void run() throws MLIOException {
				_items.clear();
			}
		});
	}
	
	/**
	 * Write the data in the underlying file
	 */
	public void export() throws IOException
	{
		try {
			export(new Controller());
		}
		catch(Controller.CanceledByUser err) {
			throw new RuntimeException("A Matlab file export operation have been unexpectedly interrupted");
		}
	}
	
	/**
	 * Write the data in the underlying file
	 */
	public void export(Controller controller) throws IOException, Controller.CanceledByUser
	{
		_lock.lock();
		Set<String> toBeRemoved = new HashSet<String>();
		try {
			controller.startCounter(_items.size());
			for(Item item : _items.values()) {
				item.append(_writer, controller);
				toBeRemoved.add(item.getMeta().getName());
			}
			controller.stopCounter();
		}
		finally {
			for(String key : toBeRemoved) {
				_items.remove(key);
			}
			_lock.unlock();
		}
	}
	
	/**
	 * Retrieve the given item and throw an exception if it does not exist
	 */
	private Item retrieveItem(String name) throws MLIOException
	{
		Item item = _items.get(name);
		if(item==null) {
			throw new MLIOException("Cannot find variable named " + name);
		}
		return item;
	}
	
	/**
	 * Execute a method while holding the lock (return immediately if already locked)
	 */
	private void tryLock(WriteOperation method) throws MLIOException
	{
		if(!_lock.tryLock()) {
			throw new MLIOException(
				"Cannot modify the MatlabExporter object while the previous export operation is not finished"
			);
		}
		try {
			method.run();
		}
		finally {
			_lock.unlock();
		}
	}
	
	/**
	 * Compute the dimension of the matlab object corresponding to the given sequence
	 * and dimension mapping scheme
	 */
	private static int[] computeSize(DimensionMapping mapping, boolean isComplex, Sequence data)
		throws MLIOException
	{
		mapping.ensureValidMapping();
		if(isComplex && (data.getSizeC()%2 != 0)) {
			throw new MLIOException("Complex sequences must have an even number of channels");
		}
		int[] rawRetVal = new int[5];
		rawRetVal[mapping.getDimensionX()] = data.getSizeX();
		rawRetVal[mapping.getDimensionY()] = data.getSizeY();
		rawRetVal[mapping.getDimensionZ()] = data.getSizeZ();
		rawRetVal[mapping.getDimensionT()] = data.getSizeT();
		rawRetVal[mapping.getDimensionC()] = isComplex ? data.getSizeC()/2 : data.getSizeC();
		int retValLength = 5;
		for(int k=4; k>=2; --k) {
			if(rawRetVal[k]==1) {
				retValLength = k;
			}
			else {
				break;
			}
		}
		int[] retVal = new int[retValLength];
		for(int k=0; k<retValLength; ++k) {
			retVal[k] = rawRetVal[k];
		}
		return retVal;
	}
	
	/**
	 * Return the Matlab type suitable for storing a sequence having the given Icy datatype
	 */
	private static MLType computeType(DataType icyType) throws MLIOException
	{
		switch(icyType) {
			case DOUBLE: return MLType.DOUBLE;
			case FLOAT : return MLType.SINGLE;
			case BYTE  : return MLType.INT8  ;
			case UBYTE : return MLType.UINT8 ;
			case SHORT : return MLType.INT16 ;
			case USHORT: return MLType.UINT16;
			case INT   : return MLType.INT32 ;
			case UINT  : return MLType.UINT32;
			default:
				throw new MLIOException("Unsupported Icy type: " + icyType);
		}
	}
	
	/**
	 * Remove all the forbidden characters in the given string so that it could
	 * be admissible as a Matlab variable name
	 */
	private static String computeName(String rawName)
	{
		String retVal = "";
		for(int k=0; k<rawName.length(); ++k) {
			char c = rawName.charAt(k);
			if((c>='a' && c<='z') || (c>='A' && c<='Z') || (c>='0' && c<='9' && k>0))
				retVal += c;
			else
				retVal += '_';
		}
		return retVal;
	}
	
	/**
	 * Interface to implement for the tryLock method
	 */
	private interface WriteOperation
	{
		/**
		 * Method called by tryLock
		 */
		public void run() throws MLIOException;
	}
	
	/**
	 * Item contained in the file
	 */
	private static class Item
	{
		private Sequence _data;
		private MLMeta _meta;
		private DimensionMapping _mapping;
		
		/**
		 * Constructor
		 */
		public Item(Sequence data, String name, DimensionMapping mapping, boolean isComplex)
			throws MLIOException
		{
			_data = data;
			refresh(name, mapping, isComplex);
		}
		
		/**
		 * Meta-data associated to this item
		 */
		public MLMeta getMeta()
		{
			return _meta;
		}
		
		/**
		 * Change the name
		 */
		public void setName(String newName) throws MLIOException
		{
			refresh(newName, _mapping, _meta.getIsComplex());
		}
		
		/**
		 * Change the dimension mapping
		 */
		public void setDimensionMapping(DimensionMapping newMapping) throws MLIOException
		{
			refresh(_meta.getName(), newMapping, _meta.getIsComplex());
		}
		
		/**
		 * Change the complex flag
		 */
		public void setIsComplex(boolean newIsComplex) throws MLIOException
		{
			refresh(_meta.getName(), _mapping, newIsComplex);
		}
		
		/**
		 * Save the item at the end of the given file
		 */
		public void append(MatFileWriter file, Controller controller)
			throws IOException, Controller.CanceledByUser
		{
			controller.checkPoint();
			MLOStream stream = file.putDataAsStream(_meta);
			int nbSteps = _meta.getIsComplex() ? 2*_meta.getSize() : _meta.getSize();
			controller.startCounter(nbSteps);
			if(_meta.getIsComplex()) {
				feedStream(stream, Cursor.ModeC.EVEN_C, controller);
				feedStream(stream, Cursor.ModeC.ODD_C , controller);
			}
			else {
				feedStream(stream, Cursor.ModeC.ALL_C, controller);
			}
			controller.stopCounter();
		}
		
		/**
		 * Feed an output stream (one-pass)
		 */
		private void feedStream(MLOStream target, Cursor.ModeC modeC, Controller controller)
			throws IOException, Controller.CanceledByUser
		{
			for(Cursor curs=Cursors.create(_data, _mapping, modeC, target); curs.isValidPosition(); curs.next()) {
				controller.checkPointNext();
				curs.consume();
			}
		}
		
		/**
		 * Create a MLMeta object for current sequence with the suitable name
		 */
		private void refresh(String rawName, DimensionMapping mapping, boolean isComplex) throws MLIOException
		{
			MLType type = computeType(_data.getDataType_());
			String name = computeName(rawName);
			int[] dimensions = computeSize(mapping, isComplex, _data);
			_meta = new MLMeta(type, name, dimensions, isComplex);
			_mapping = mapping;
		}
	}
}
